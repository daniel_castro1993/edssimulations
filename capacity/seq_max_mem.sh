#/bin/bash

cd ./bin

for i in 410 420 430 440 450 460 470 480 490 500 510 520
do
  for p in {1..30}
  do
    ./Capacity L1_PADDING $p L $i SAMPLES 10000 &
  done
  wait ;
done

wait ;
