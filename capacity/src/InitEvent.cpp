#include <set>

#include "app.hpp"

void InitEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
    ThreadProcess* tp = dynamic_cast<ThreadProcess*> (lp);
    SimState* st = dynamic_cast<SimState*> (state);
    RandomLib *r = RandomLib::getInstance(0);
    double next_ts = NEXT_TIMESTAMP(now);
    AcquireEvent *acqEV;
    int i, j, g, L1_cleanup_size, LLC_cleanup_size;
    int L1_cleaned = 0, LLC_cleaned = 0, count_meta = 0;
    ACCESS_T access;
    set<granule_array_s*>::iterator it;

    if (tp->nb_sample >= st->tot_nb_samples && !tp->IsEnd()) {
        printf("[LP %i] total: %i, commits: %i, aborts: %i\n",
               lp->GetId(), tp->nb_sample, tp->nb_commits, tp->nb_aborts);
        tp->End();
    }

    tp->nb_sample++;

    new_granules(tp, r, st->avg_acquisitions, st->tot_nb_granules);

    L1_cleanup_size = tp->L1_used.size();
    for (it = tp->L1_used.begin(); it != tp->L1_used.end(); ++it) {
        for (i = 0; i < st->nb_L1_slots; ++i) {
            access = (*it)->array[i].access;
            if (access == WRITE || access == READ) {
                (*it)->array[i].access = NONE;
                (*it)->count--;
                L1_cleaned++;
            }
            else if (access == META) {
                count_meta++;

#ifdef RANDOM_PADDING
                (*it)->array[i].access = NONE;
                (*it)->count--;
#endif /* RANDOM_PADDING */
            }
        }
    }
    tp->L1_used.clear();

#ifdef RANDOM_PADDING
    for (i = 0; i < count_meta; ++i) {
        int rand_pad = r->UniformIntDist(0, st->nb_L1_sets - 1);
        for (j = 0; j < st->nb_L1_slots; ++j) {
            if (tp->L1_cache[rand_pad].array[j].access == NONE) {
                tp->L1_cache[rand_pad].array[j].access = META;
                tp->L1_cache[rand_pad].count++;
                break;
            }
        }
    }
#endif /* RANDOM_PADDING */

#ifdef SIMULATE_LLC
    count_meta = 0;

    //	printf("L1 Sets to cleanup: %2i, L1 CL cleaned: %3i || ",
    //		L1_cleanup_size, L1_cleaned);

    LLC_cleanup_size = tp->LLC_used.size();
    for (it = tp->LLC_used.begin(); it != tp->LLC_used.end(); ++it) {
        for (i = 0; i < st->nb_LLC_slots; ++i) {
            access = (*it)->array[i].access;
            if (access == WRITE || access == READ) {

                (*it)->array[i].access = NONE;
                (*it)->count--;
                LLC_cleaned++;
            }
            else if (access == META) {
                count_meta++;
            }
        }
    }
    tp->LLC_used.clear();
#endif

    //	if (count_meta != st->LLC_padding) {
    //		printf("LLC METAs are running!\n");
    //	}

    //	printf("LLC Sets to cleanup: %4i, LLC CL cleaned: %6i\n",
    //		LLC_cleanup_size, LLC_cleaned);

    acqEV = NEW_EVENT(AcquireEvent,
                      lp->GetId(),
                      next_ts,
                      E_TAG_ACQUIRE);

    g = r->UniformIntDist(1, st->tot_nb_granules);
    access = r->BernoulliDist(st->prob_write) ? WRITE : READ;

    acqEV->g = g;
    acqEV->access = access;

    schedule_event(lp->GetId(), acqEV);
}
