#include "app.hpp"
#include "utils.hpp"

static bool store(int, ThreadProcess*, SimState*);
static bool load(int, ThreadProcess*, SimState*);
static int map_policy(bool, int, ThreadProcess*, SimState*);
static int slice_policy(int, ThreadProcess*, SimState*);
static granule_array_s* find_set(bool, int, ThreadProcess*, SimState*);
static granule_s* evict_policy(bool, granule_s*, SimState*);
static bool is_abort(bool, granule_s*, SimState*);

void AcquireEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
    ThreadProcess* tp = dynamic_cast<ThreadProcess*> (lp);
    SimState* st = dynamic_cast<SimState*> (state);
    RandomLib *r = RandomLib::getInstance(0);

    double lat = r->ExpDist(1.0 / tp->per_granule_time);
    double next_ts = NEXT_TIMESTAMP(now + lat);
    int cur_nb_acqs = tp->acquired.size();
    int next_g;
    ACCESS_T next_access;

    AcquireEvent *acqEV;
    InitEvent *initEV;

    if (cur_nb_acqs == tp->nb_acqs) {
        // COMMIT
        // no more accesses to do

        tp->nb_commits++;
        tp->acquired.clear();

        initEV = NEW_EVENT(InitEvent,
                           lp->GetId(),
                           NEXT_TIMESTAMP(now),
                           0);
        initEV->is_abort = false;

        schedule_event(lp->GetId(), initEV);
        return;
    }

    bool spurious_abort = false;

    // more accesses to do
#ifdef BLOOM_FILTER
    double exp_coeff = st->bf_k * (double) cur_nb_acqs / st->bf_m;
    spurious_abort =
            r->BernoulliDist(pow(1.0 - exp(-exp_coeff), st->bf_k));
#else
    spurious_abort = access == WRITE
            ? r->BernoulliDist(st->prob_spurious_w)
            : r->BernoulliDist(st->prob_spurious_r);
#endif // DISABLE_BLOOM_FILTER

    bool access_res;

    if (access == WRITE) {
        access_res = store(g, tp, st);
    }
    else {
        access_res = load(g, tp, st);
    }

    if (!spurious_abort && access_res) {

        next_access = r->BernoulliDist(st->prob_write) ? WRITE : READ;
#ifdef SEQUENTIAL
        next_g = g + 1;
#else
        do {
            next_g = r->UniformIntDist(1, st->tot_nb_granules);
        }
        while (tp->acquired.find(next_g) != tp->acquired.end());
#endif

        tp->acquired[g] = access;

        acqEV = NEW_EVENT(AcquireEvent,
                          lp->GetId(),
                          next_ts,
                          E_TAG_ACQUIRE);
        acqEV->g = next_g;
        acqEV->access = next_access;
        schedule_event(lp->GetId(), acqEV);
        return;
    }

    // ABORT
    // spurious or eviction
    tp->nb_aborts++;
    tp->acquired.clear();

    initEV = NEW_EVENT(InitEvent,
                       lp->GetId(),
                       NEXT_TIMESTAMP(now),
                       0);
    initEV->is_abort = true;

    schedule_event(lp->GetId(), initEV);
}

static bool access_g(granule_array_s *set,
                     ACCESS_T access,
                     int slots,
                     simtime_s ts)
{
    int i;

    if (set->count < slots) {
        // no evict
        set->count++;
        for (i = 0; i < slots; ++i) {
            if (set->array[i].access == NONE) {
                set->array[i].access = access;
                set->array[i].timestamp = ts;
                break;
            }
        }
        return true;
    }
    return false;
}

static bool access_addr(int addr,
                        ThreadProcess *tp,
                        SimState *st,
                        ACCESS_T access)
{
    RandomLib *r = RandomLib::getInstance(0);
    int slots_L1 = st->nb_L1_slots;
    granule_array_s *L1_set = find_set(true, addr, tp, st);

    granule_s *evict;
    simtime_s ts = tp->GetCurrentTime();
    bool access_L1 = access_g(L1_set, access, slots_L1, ts);
    bool evict_abort;

#ifdef SIMULATE_LLC 
    bool do_extra;
    int i;
    granule_array_s *LLC_set = find_set(false, addr, tp, st);
    int slots_LLC = st->nb_LLC_slots;
    bool access_LLC = access_g(LLC_set, access, slots_LLC, ts);

    tp->LLC_used.insert(LLC_set);
    do_extra = r->BernoulliDist(st->prob_extra);

    if (do_extra) {
        for (i = 0; i < st->LLC_extra_reads; ++i) {
            int ex_addr = r->UniformIntDist(0, st->nb_LLC_sets
                                            * st->nb_LLC_slots);
            granule_array_s *LLC_ex_set = find_set(false, ex_addr, tp, st);
            tp->LLC_used.insert(LLC_ex_set);
            access_g(LLC_ex_set, READ, slots_LLC, ts);
        }
    }
#endif

    tp->L1_used.insert(L1_set);

    if (access_L1
#ifdef SIMULATE_LLC
            && access_LLC
#endif
            ) {
        return true;
    }

    // have to evict
    evict = evict_policy(true, L1_set->array, st);
    evict_abort = is_abort(true, evict, st);

    if (evict_abort) {
        return false;
    }

    // the eviction did not abort
    if (evict->access != META) {
        evict->access = access;
    }
    evict->timestamp = ts;

#ifdef SIMULATE_LLC
    if (access_LLC) {
        return true;
    }

    // have to evict from LLC (should always cause the abort)
    evict = evict_policy(false, LLC_set->array, st);
    evict_abort = is_abort(false, evict, st);
#endif
    return !evict_abort;
}

static bool store(int addr, ThreadProcess *tp, SimState * st)
{
    return access_addr(addr, tp, st, WRITE);
}

static bool load(int addr, ThreadProcess *tp, SimState * st)
{
    return access_addr(addr, tp, st, READ);
}

static int map_policy(bool search_L1,
                      int addr,
                      ThreadProcess*,
                      SimState * st)
{
    int addr_map;
    addr_map = addr % st->nb_L1_sets;

#ifdef SIMULATE_LLC
    if (!search_L1) {
        addr_map = addr % st->nb_LLC_sets;
    }
#endif

    return addr_map;
}

static int slice_policy(int addr,
                        ThreadProcess *tp,
                        SimState * st)
{
    // slice of the LLC
    /* RandomLib *rand = RandomLib::getInstance(0);
    return rand->UniformIntDist(0, st->nb_cores - 1); */
    if (st->nb_cores == 4) {
        int h1[] = {18, 19, 21, 23, 25, 27, 29, 30, 31};
        int h2[] = {17, 19, 20, 21, 22, 23, 24, 26, 28, 29, 31};
        int h1_size = 9, h2_size = 11;
        int i, bit1 = (addr >> h1[0]) & 1, bit2 = (addr >> h2[0]) & 1;

        for (i = 1; i < h1_size; ++i) {
            bit1 ^= (addr >> h1[i]) & 1;
        }
        for (i = 1; i < h2_size; ++i) {
            bit2 ^= (addr >> h2[i]) & 1;
        }

        return (bit2 << 1) | (bit1); // only works for 4 cores
    }
    else {
        RandomLib *rand = RandomLib::getInstance(0);
        return rand->UniformIntDist(0, st->nb_cores - 1);
    }
}

static granule_array_s * find_set(bool search_L1,
                                  int addr,
                                  ThreadProcess *tp,
                                  SimState * state)
{
    granule_array_s *res;
    int addr_map = map_policy(search_L1, addr, tp, state);

    res = &(tp->L1_cache[addr_map]);
#ifdef SIMULATE_LLC
    if (!search_L1) {
        int slice = slice_policy(addr, tp, state);
        res = &(state->LLC_cache[slice][addr_map]);
    }
#endif

    return res;
}

static granule_s * evict_policy(bool L1_evict,
                                granule_s *set,
                                SimState * state)
{
    RandomLib *rand = RandomLib::getInstance(0);
    bool evict_read = rand->BernoulliDist(state->prob_evict_read);
    int set_size = state->nb_L1_slots;

#ifdef SIMULATE_LLC
    if (!L1_evict) {
        state->nb_LLC_slots;
    }
#endif

    simtime_s ts = set[0].timestamp;

    granule_s *evict = &(set[0]);
    granule_s *read = NULL;

    int i;
    for (i = 1; i < set_size; ++i) {
        //-- comment to evict meta and abort
        //		if (evict->access == META) {
        //			evict = &(set[i]);
        //			continue;
        //		}
        //--
        if ((read == NULL || read->timestamp < set[i].timestamp)
            && set[i].access == READ) {
            read = &(set[i]);
        }
        if (set[i].timestamp < evict->timestamp /* && set[i].access != META*/) {
            evict = &(set[i]);
        }
        if (set[i].timestamp > ts) {
            ts = set[i].timestamp;
        }
    }

    if (evict_read && read != NULL && evict->access == WRITE) {
        read->timestamp = ts;
        evict = read;
    }
    return evict;

    // -- random policy
    //	int g_idx = rand->UniformIntDist(0, set_size - 1);
    //	return &(set[g_idx]);
}

static bool is_abort(bool L1_evict,
                     granule_s *evict,
                     SimState * state)
{
    bool ab_r, ab_w;
    RandomLib *rand = RandomLib::getInstance(0);

    if (!L1_evict) {
        // LLC evict
        return true;
    }

    ab_r = rand->BernoulliDist(state->prob_ab_ev_r);
    ab_w = rand->BernoulliDist(state->prob_ab_ev_w);

    return evict->access == META || (evict->access == WRITE && ab_w)
            || (evict->access == READ && ab_r);
}
