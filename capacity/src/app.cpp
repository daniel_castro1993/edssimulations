#include "app.hpp"

#include <cstdlib>
#include <ctime>
#include <cstdlib>
#include <cstdio> 

using namespace sim;
using namespace std;

void init(map<string, string>& args)
{
    ThreadProcess *tLP;
    InitEvent *initEV;
    SimState *state = new SimState();
    int i, j, k, pad, LLC_pad = 0;
    lpid_s lpid;

    SET_STATE(SimState, state);

    srand(clock());

    state->seed = rand();
    state->nb_L1_sets = 64;
    state->nb_L1_slots = 8;
    state->nb_cores = 4;
    state->tot_nb_granules = 1000;
    state->tot_nb_samples = 10;
    state->avg_acquisitions = 10;
    state->prob_write = 1.0;
    state->L1_padding = 0;
#ifdef SIMULATE_LLC
    state->nb_LLC_sets = 2048;
    state->nb_LLC_slots = 16;
    state->LLC_padding = 0;
    state->LLC_extra_reads = 0;
#endif
    state->prob_evict_read = 0;
    state->prob_spurious_r = 0;
    state->prob_spurious_w = 0;
    state->prob_ab_ev_r = 0;
    state->prob_ab_ev_w = 1;
    state->bf_k = 5;
    state->bf_m = 2048;

    state->file_name = "capacity.txt";

    if (!args["DEFAULT_SEED"].empty()) {
        state->seed = stoi(args["DEFAULT_SEED"]);
    }

    if (!args["PW"].empty()) {
        state->prob_write = stod(args["PW"]);
    }

    if (!args["PROB_READ"].empty()) {
        state->prob_write = 1.0 - stod(args["PROB_READ"]);
    }

    if (!args["D"].empty()) {
        state->tot_nb_granules = stoi(args["D"]);
    }

    if (!args["NB_L1_SETS"].empty()) {
        state->nb_L1_sets = stoi(args["NB_L1_SETS"]);
    }

    if (!args["NB_L1_SLOTS"].empty()) {
        state->nb_L1_slots = stoi(args["NB_L1_SLOTS"]);
    }

#ifdef SIMULATE_LLC
    if (!args["NB_LLC_SETS"].empty()) {
        state->nb_LLC_sets = stoi(args["NB_LLC_SETS"]);
    }

    if (!args["NB_LLC_SLOTS"].empty()) {
        state->nb_LLC_slots = stoi(args["NB_LLC_SLOTS"]);
    }

    if (!args["LLC_PADDING"].empty()) {
        state->LLC_padding = stoi(args["LLC_PADDING"]);
    }

    if (!args["LLC_EXTRA_READS"].empty()) {
        state->LLC_extra_reads = stoi(args["LLC_EXTRA_READS"]);
    }

    if (!args["LLC_EXTRA_READS"].empty()) {
        state->LLC_extra_reads = stoi(args["LLC_EXTRA_READS"]);
    }

    if (!args["LLC_P_EXTRA"].empty()) {
        state->prob_extra = stod(args["LLC_P_EXTRA"]);
    }
#endif

    if (!args["NB_CORES"].empty()) {
        state->nb_cores = stoi(args["NB_CORES"]);
    }

    if (!args["L"].empty()) {
        state->avg_acquisitions = stod(args["L"]);
    }

    if (!args["P_EVICT_R"].empty()) {
        state->prob_evict_read = stod(args["P_EVICT_R"]);
    }

    if (!args["P_AB_EV_R"].empty()) {
        state->prob_ab_ev_r = stod(args["P_AB_EV_R"]);
    }

    if (!args["P_AB_EV_W"].empty()) {
        state->prob_ab_ev_w = stod(args["P_AB_EV_W"]);
    }

    if (!args["P_SPURIOUS_R"].empty()) {
        state->prob_spurious_r = stod(args["P_SPURIOUS_R"]);
    }

    if (!args["P_SPURIOUS_W"].empty()) {
        state->prob_spurious_w = stod(args["P_SPURIOUS_W"]);
    }

    if (!args["SAMPLES"].empty()) {
        state->tot_nb_samples = stoi(args["SAMPLES"]);
    }

    if (!args["L1_PADDING"].empty()) {
        state->L1_padding = stoi(args["L1_PADDING"]);
    }

    if (!args["BF_K"].empty()) {
        state->bf_k = stoi(args["BF_K"]);
    }

    if (!args["BF_M"].empty()) {
        state->bf_m = stoi(args["BF_M"]);
    }

    if (!args["FILE"].empty()) {
        state->file_name = args["FILE"];
    }

    if (state->tot_nb_granules < state->avg_acquisitions) {
        printf("D must be greater than L\n");
        exit(EXIT_FAILURE);
    }

    printf("Started simulation with parameters: \nDEFAULT_SEED = %i\n"
           "P_W = %.2f\nPOOL_SIZE = %i\nAVG_ACQS = %f\nNB_CORES = %i\n"
           "NB_L1_SETS = %i\nNB_L1_SLOTS = %i\nL1_PADDING=%i\n"
           "PROB_EXTRA=%f\nBF_M=%f\nBF_K=%f\nSAMPLES = %i\n",
           state->seed, state->prob_write, state->tot_nb_granules,
           state->avg_acquisitions, state->nb_cores, state->nb_L1_sets,
           state->nb_L1_slots, state->L1_padding, state->prob_extra,
           state->bf_m, state->bf_k, state->tot_nb_samples);

#ifdef SIMULATE_LLC
    printf("Started simulation with parameters: \nNB_LLC_SETS = %i\n"
           "NB_LLC_SLOTS = %i\nLLC_PADDING=%i\nLLC_EXTRA_READS=%i\n",
           state->nb_LLC_sets, state->nb_LLC_slots,
           state->LLC_padding, state->LLC_extra_reads);
#endif

    RandomLib::create(state->seed);
    RandomLib *r = RandomLib::getInstance(0);

    tLP = new ThreadProcess();
    tLP->ended = false;
    tLP->nb_aborts = 0;
    tLP->nb_commits = 0;
    tLP->nb_sample = 0;

    // allocate L1
    tLP->L1_cache = (granule_array_s*)
            malloc(sizeof (granule_array_s) * state->nb_L1_sets);
    for (i = 0; i < state->nb_L1_sets; ++i) {
        tLP->L1_cache[i].array = (granule_s*)
                malloc(sizeof (granule_s) * state->nb_L1_slots);
        tLP->L1_cache[i].count = 0;
        for (j = 0; j < state->nb_L1_slots; ++j) {
            tLP->L1_cache[i].array[j].access = NONE;
        }
    }

    // add L1 PADDING
    pad = state->L1_padding;
    i = 0;
    while (pad-- > 0) {
#ifdef RANDOM_PADDING
        int rand_pad = r->UniformIntDist(0, state->nb_L1_sets - 1);
        for (j = 0; j < state->nb_L1_slots; ++j) {
            if (tLP->L1_cache[rand_pad].array[j].access == NONE) {
                tLP->L1_cache[rand_pad].array[j].access = META;
                tLP->L1_cache[rand_pad].count++;
                break;
            }
        }
#else
        for (j = 0; j < state->nb_L1_slots; ++j) {
            if (tLP->L1_cache[i].array[j].access == NONE) {
                tLP->L1_cache[i].array[j].access = META;
                tLP->L1_cache[i].count++;
                break;
            }
        }
#endif
        i++;
        i = i % state->nb_L1_sets;
    }

#ifdef SIMULATE_LLC
    // allocate LLC
    state->LLC_cache = (granule_array_s**)
            malloc(sizeof (granule_array_s*) * state->nb_cores);
    for (j = 0; j < state->nb_cores; ++j) {
        state->LLC_cache[j] = (granule_array_s*)
                malloc(sizeof (granule_array_s) * state->nb_LLC_sets);
        for (i = 0; i < state->nb_LLC_sets; ++i) {
            state->LLC_cache[j][i].array = (granule_s*)
                    malloc(sizeof (granule_s) * state->nb_LLC_slots);
            state->LLC_cache[j][i].count = 0;
            for (k = 0; k < state->nb_LLC_slots; ++k) {
                state->LLC_cache[j][i].array[k].access = NONE;
            }
        }
    }

    // add LLC PADDING
    pad = state->LLC_padding;
    while (pad-- > 0) {
        int slice, set;

        slice = r->UniformIntDist(0, state->nb_cores - 1);
        set = r->UniformIntDist(0, state->nb_LLC_sets - 1);

        for (i = 0; i < state->nb_LLC_slots; ++i) {
            if (state->LLC_cache[slice][set].array[i].access == NONE) {
                state->LLC_cache[slice][set].array[i].access = META;
                state->LLC_cache[slice][set].count++;
                LLC_pad++;
                break;
            }
        }
    }

    printf("LLC actual padding: %i\n", LLC_pad);
#endif

    lpid = add_logical_process(tLP);
    initEV = new InitEvent(lpid, NEXT_TIMESTAMP(0));
    initEV->is_abort = false;
    schedule_event(lpid, initEV);
}

void end(vector<LogicalProcess*> lps, GlobalState *state)
{
    vector<LogicalProcess*>::iterator it,
            begin = lps.begin(),
            end = lps.end();
    int tot_commits = 0, tot_aborts = 0;
    double prob_abort = 0;

    SimState *st = dynamic_cast<SimState*> (state);

    st->file_ptr = fopen(st->file_name.c_str(), "a");
    fseek(st->file_ptr, 0, SEEK_END);
    if (ftell(st->file_ptr) < 8) {
        fprintf(st->file_ptr, "%12s,%12s,%12s,%12s,%12s,%12s,%12s,%12s,%12s,"
                "%12s", "SEED", "SAMPLES", "PW", "D", "L", "L1_PADDING",
                "NB_CORES", "NB_L1_SETS", "NB_L1_SLOTS", "P_A (%)");
#ifdef SIMULATE_LLC
        fprintf(st->file_ptr, "%12s,%12s,%12s,%12s", "LLC_padding",
                "LLC_extra_reads", "nb_LLC_sets", "nb_LLC_slots");
#endif
        fprintf(st->file_ptr, "\n");
    }
    for (it = begin; it != end; ++it) {
        ThreadProcess* tp = dynamic_cast<ThreadProcess*> (*it);
        tot_commits += tp->nb_commits;
        tot_aborts += tp->nb_aborts;
    }
    prob_abort = (double) tot_aborts / (double) (tot_aborts + tot_commits);

    fprintf(st->file_ptr, "%12i,%12i,%12f,%12i,%12.2f,"
            "%12i,%12i,%12i,%12i,%12f\n", st->seed,
            st->tot_nb_samples, st->prob_write, st->tot_nb_granules,
            st->avg_acquisitions, st->L1_padding, st->nb_cores, st->nb_L1_sets,
            st->nb_L1_slots, prob_abort * 100.0);
#ifdef SIMULATE_LLC
    fprintf(st->file_ptr, "%12i,%12i,%12i,%12i", st->LLC_padding,
            st->LLC_extra_reads, st->nb_LLC_sets, st->nb_LLC_slots);
#endif
    fprintf(st->file_ptr, "\n");
    fflush(st->file_ptr);
    fclose(st->file_ptr);
}
