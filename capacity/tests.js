const fs = require('fs');
const util = require('util');
const os = require('os');
const spawn = require('child_process').spawn;

var cpu_count = os.cpus().length;

/*
var POOL_SIZE = 1048576;
var SAMPLES = 5000;
var P_W = [1.0, 0.9, 0.9995, 0.999, 0.998, 0.99, 0.95, 0.5, 0.1, 0.05, 0.01,
           0.002, 0.001, 0.0005, 0.0];
var ACQS = [50,  100, 150, 175, 200, 210, 220, 230, 240, 250, 275,
            300, 325, 350, 375, 400, 500, 600, 700, 800, 900, 1000];
*/

var concurrent_procs = 5;

var POOL_SIZE = 1048576;
var SAMPLES = 10000;
var SPURIOUS_R = 0; //.00006;
var SPURIOUS_W = 0; //.00006;
var AB_EV_R = 0; // 0.00005;
var AB_EV_W = 1;
var P_EVICT_R = 0;
var L1_PADDING = 0; // 30;
var LLC_PADDING = 0; //15000; // TODO;
var LLC_EXTRA_READS = 0; // TODO;
var LLC_P_EXTRA_READS = 0; // TODO;
var NB_CORES = 4;
var NB_L1_SETS = 64;
var NB_L1_SLOTS = 8;
var NB_LLC_SETS = 2048;
var NB_LLC_SLOTS = 16;
var BF_K = 10;
var BF_M = 131072;

var P_W = [1.0]; // 1, 0.9, 0.5, 0.1, 0.01, 
// var P_W = [1.0, 0.9, 0.9995, 0.999, 0.998, 0.99, 0.95, 0.5, 0.1, 0.05, 0.01,
//           0.002, 0.001, 0.0005, 0.0];
// var ACQS = [50000, 1000, 40000, 30000, 5000, 10000, 20000 ];
// var ACQS = [8000, 12000, 15000, 18000];
var ACQS = [50, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400,
    450, 500, 600, 700, 800, 900, 1000, 1200, 1400, 1600, 1800, 2000, 2500,
    3000, 4000, 5000];
// var ACQS = [50,  100, 150, 175, 200, 210, 220, 230, 240, 250, 275,
//            300, 325, 350, 375, 400, 500, 600, 700, 800, 900, 1000]

var current = {
    P_W: 0,
    ACQS: 0
};

var files = [];

var exit = false;
var ended = 0;

function update() {
    current.ACQS = (current.ACQS + 1) % ACQS.length;
    if (current.ACQS == 0) {
        current.P_W += 1;
        if (current.P_W >= P_W.length) {
            // exit
            exit = true;
        }
    }
}

function recCapacitySpawn(capacity) {
    capacity.on('close', (code) => {
        capacitySpawn();
    });
}

function capacitySpawn() {
    if (exit) {
        console.log("Computation ended. Dealling with generated files.");
        mergeFiles();
        return null;
    }
    var file = "cap-PW" + P_W[current.P_W]
            + "-ACQ" + ACQS[current.ACQS];
    files.push(file);
    console.log(util.format("P_W=%d ACQS=%d", P_W[current.P_W], ACQS[current.ACQS]));
    var capacity = spawn('./Capacity',
        [
            'SAMPLES', SAMPLES,
            'D', POOL_SIZE,
            'P_SPURIOUS_R', SPURIOUS_R,
            'P_SPURIOUS_W', SPURIOUS_W,
            'P_AB_EV_R', AB_EV_R,
            'P_AB_EV_W', AB_EV_W,
            'P_EVICT_R', P_EVICT_R,
            'L1_PADDING', L1_PADDING,
            'LLC_PADDING', LLC_PADDING,
            'LLC_EXTRA_READS', LLC_EXTRA_READS,
            'LLC_P_EXTRA', LLC_P_EXTRA_READS,
            'NB_CORES', NB_CORES,
            'NB_L1_SETS', NB_L1_SETS,
            'NB_L1_SLOTS', NB_L1_SLOTS,
            'NB_LLC_SETS', NB_LLC_SETS,
            'NB_LLC_SLOTS', NB_LLC_SLOTS,
            'BF_M', BF_M,
            'BF_K', BF_K,
            'PW', P_W[current.P_W],
            'L', ACQS[current.ACQS],
            'FILE', file
        ], {
            detached: true,
            stdio: 'ignore'
        });
    update();
    recCapacitySpawn(capacity);
}

function mergeFiles() {
    ended += 1;
    if (ended < concurrent_procs) {
        return;
    }
    var merged_file_name = "merged_cap_tests.txt";
    try {
        fs.unlinkSync(merged_file_name);
        console.log("replaced old merged_cap_tests.txt.");
    } catch (e) {
        console.log("merged_cap_tests.txt did not exist.");
    }

    for (var i = 0; i < files.length; ++i) {
        file = files[i];
        var text = fs.readFileSync(file, 'utf8');
        var secLine = text.split("\n")[1];
        if (i == 0) {
            fs.writeFileSync(merged_file_name, text, 'utf8');
        } else {
            fs.appendFileSync(merged_file_name, secLine + '\n', 'utf8');
        }
        fs.unlinkSync(file);
    }
}

process.chdir('./bin');
for (var i = 0; i < concurrent_procs; ++i) {
    if (!exit) {
        capacitySpawn();
    }
}
