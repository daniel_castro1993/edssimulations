const fs = require('fs');
const util = require('util');
const os = require('os');
const spawn = require('child_process').spawn;
const parse = require('csv-parse');

var cpu_count = os.cpus().length;

const HAR_FILE_PATH = "./results_wo.txt";
const ALPHA = 0.7;
const MAX_ATTEMPTS = 10;

/*
var POOL_SIZE = 1048576;
var SAMPLES = 5000;
var P_W = [1.0, 0.9, 0.9995, 0.999, 0.998, 0.99, 0.95, 0.5, 0.1, 0.05, 0.01,
           0.002, 0.001, 0.0005, 0.0];
var ACQS = [50,  100, 150, 175, 200, 210, 220, 230, 240, 250, 275,
            300, 325, 350, 375, 400, 500, 600, 700, 800, 900, 1000];
*/

var concurrent_procs = 4;

var hardware_data = {};
var P_W = [];
var ACQS = [];

// err -> real - estimated
// if err < 0 : Over-estimate
// if err > 0 : Under-estimate

// TODO: check how to do multi-dimentions gradient descend
var test_params = {
    inc_L1_PADDING: 50,
    L1_PADDING: {
        on_error: function (err) {
            console.log("Error: " + err);
            if (err < 0) {
                L1_PADDING -= test_params.inc_L1_PADDING;
            } else {
                L1_PADDING += test_params.inc_L1_PADDING;
            }
            if (L1_PADDING < 0) {
                L1_PADDING = 0;
            }
            test_params.inc_L1_PADDING *= ALPHA; // reduction factor
            test_params.inc_L1_PADDING = parseInt(test_params.inc_L1_PADDING);
            if (test_params.inc_L1_PADDING < 1) {
                test_params.inc_L1_PADDING = 1;
            }
            console.log("New_L1_PADDING: " + L1_PADDING);
        }
    },
    inc_L3_PADDING: 50000,
    L3_PADDING: {
        on_error: function (err) {
            console.log("Error: " + err);
            if (err < 0) {
                LLC_PADDING -= test_params.inc_L3_PADDING;
            } else {
                LLC_PADDING += test_params.inc_L3_PADDING;
            }
            if (LLC_PADDING < 0) {
                LLC_PADDING = 0;
            }
            test_params.inc_L3_PADDING *= ALPHA; // reduction factor
            test_params.inc_L3_PADDING = parseInt(test_params.inc_L3_PADDING);
            if (test_params.inc_L3_PADDING < 1) {
                test_params.inc_L3_PADDING = 1;
            }
            console.log("New_L3_PADDING: " + LLC_PADDING);
        }
    },
    inc_M: 100000,
    M: {
        on_error: function (err) {
            console.log("Error: " + err);
            if (err < 0) {
                BF_M += test_params.inc_M;
            } else {
                BF_M -= test_params.inc_M;
            }
            if (BF_M < 0) {
                BF_M = 0;
            }
            test_params.inc_M *= ALPHA; // reduction factor
            test_params.inc_M = parseInt(test_params.inc_M);
            if (test_params.inc_M < 1) {
                test_params.inc_M = 1;
            }
            console.log("New_BF_M: " + BF_M);
        }
    }
}

// load file with the results from hardware
fs.createReadStream(HAR_FILE_PATH) // TODO: path
    .pipe(parse({delimiter: ';'}))
    .on('data', function(csvrow) {
        if(csvrow[0].trim() == "PROB_READ") {
            // first row
            return;
        }

        // TODO: standardize the file input

        var key = parseInt(parseFloat(csvrow[0]) * 10 + parseFloat(csvrow[1]) * 100000);

        console.log(key);

        if (!(P_W.indexOf(1.0 - parseFloat(csvrow[0].trim()) / 1000) > -1)) {
            P_W.push(1.0 - parseFloat(csvrow[0].trim()) / 1000);
        }

        if (!(ACQS.indexOf(csvrow[1].trim()) > -1)) {
            ACQS.push(csvrow[1].trim());
        }

        if (key in hardware_data) {
            hardware_data[key]['count'] += 1;
            hardware_data[key]['sum_success'] += parseFloat(csvrow[4].trim());
            hardware_data[key]['sum_abort'] += parseFloat(csvrow[3].trim());
            hardware_data[key]['sum_capacity'] += parseFloat(csvrow[2].trim());
        } else {
            hardware_data[key] = {
                'count'       : 1,
                'sum_success' : parseFloat(csvrow[4].trim()),
                'sum_abort'   : parseFloat(csvrow[3].trim()),
                'sum_capacity': parseFloat(csvrow[2].trim())
            };
        }
    })
    .on('end',function() {
        //do something wiht csvData
        for (key in hardware_data) {
            hardware_data[key]['capacity'] = hardware_data[key]['sum_capacity']
                / (hardware_data[key]['sum_capacity'] + hardware_data[key]['sum_success']);
        }

        console.log("ACQS: " + ACQS);
        console.log("P_W: " + P_W);

        // start
        process.chdir('./bin');
        for (var i = 0; i < concurrent_procs; ++i) {
            if (!exit) {
                capacitySpawn();
                update();
            }
        }
    });


// TODO: vary parameters

var POOL_SIZE = 1048576;
var SAMPLES = 10000;
var SPURIOUS_R = 0; //.00006;
var SPURIOUS_W = 0; //.00006;
var AB_EV_R = 0; // 0.00005;
var AB_EV_W = 1;
var P_EVICT_R = 0;
var L1_PADDING = 0; // 30;
var LLC_PADDING = 0; //15000; // TODO;
var LLC_EXTRA_READS = 0; // TODO;
var LLC_P_EXTRA_READS = 0; // TODO;
var NB_CORES = 4;
var NB_L1_SETS = 64;
var NB_L1_SLOTS = 8;
var NB_LLC_SETS = 2048;
var NB_LLC_SLOTS = 16;
var BF_K = 10;
var BF_M = 0;

var current = {
    P_W: 0,
    ACQS: 0,
    attempt: 0
};

var files = [];

var exit = false;
var end_attempt = false;
var ended = 0;

function update() {
    current.ACQS = (current.ACQS + 1) % ACQS.length;
    if (current.ACQS == 0) {
        current.P_W += 1;
        if (current.P_W >= P_W.length) {
            // compare with real values:

            current.attempt += 1;

            if (current.attempt > MAX_ATTEMPTS) {
                exit = true;
            } else {
                end_attempt = true;
                current.ACQS = 0;
                current.P_W = 0;
            }
        }
    }
}

function recCapacitySpawn(capacity) {
    capacity.on('exit', (code) => {
        update();
        if (end_attempt) {
            end_attempt = false;
            console.log("Round " + current.attempt + " ended.");
            checkResults();
        }
        capacitySpawn();
    });
}

function capacitySpawn() {
    if (exit) {
        console.log("Computation ended. Dealling with generated files.");
        mergeFiles();
        return null;
    }

    var file = "cap-PW" + P_W[current.P_W]
            + "-ACQ" + ACQS[current.ACQS];
    files.push(file);
    console.log(util.format("P_W=%d ACQS=%d", P_W[current.P_W], ACQS[current.ACQS]));
    var capacity = spawn('./Capacity',
        [
            'SAMPLES', SAMPLES,
            'D', POOL_SIZE,
            'P_SPURIOUS_R', SPURIOUS_R,
            'P_SPURIOUS_W', SPURIOUS_W,
            'P_AB_EV_R', AB_EV_R,
            'P_AB_EV_W', AB_EV_W,
            'P_EVICT_R', P_EVICT_R,
            'L1_PADDING', L1_PADDING,
            'LLC_PADDING', LLC_PADDING,
            'LLC_EXTRA_READS', LLC_EXTRA_READS,
            'LLC_P_EXTRA', LLC_P_EXTRA_READS,
            'NB_CORES', NB_CORES,
            'NB_L1_SETS', NB_L1_SETS,
            'NB_L1_SLOTS', NB_L1_SLOTS,
            'NB_LLC_SETS', NB_LLC_SETS,
            'NB_LLC_SLOTS', NB_LLC_SLOTS,
            'PW', P_W[current.P_W],
            'L', ACQS[current.ACQS],
            'BF_K', BF_K,
            'BF_M', BF_K,
            'FILE', file
        ], {
            detached: true,
            stdio: 'ignore'
        });
    recCapacitySpawn(capacity);
}

function handleCmp(res) {
    hardware_data.err = 0;
    var max_diff = 0;
    for (var i in res) {
        var key = parseInt((1.0-parseFloat(res[i][2])) * 10000 + parseFloat(res[i][4]) * 100000);
        console.log("key: " + key);
        hardware_data[key].estimated = parseFloat(res[i][13]) / 100;
        console.log("Real: " + hardware_data[key].capacity
        + " vs Est.: " +  hardware_data[key].estimated);
        var diff = hardware_data[key].capacity - hardware_data[key].estimated;
        if (Math.abs(diff) > Math.abs(max_diff)) {
            max_diff = diff;
        }
    }
    hardware_data.err = max_diff;

    // TODO: find a better way of doing this
    if (process.argv[2] == "L1_PADDING") {
        test_params.L1_PADDING.on_error(hardware_data.err);
    } else if (process.argv[2] == "LLC_PADDING") {
        test_params.L3_PADDING.on_error(hardware_data.err);
    } else if (process.argv[2] == "BLOOM_FILTER") {
        test_params.M.on_error(hardware_data.err);
    }
}

function checkResults() {
    var res = [];

    console.log("files: " + files);

    for (var i = 0; i < files.length; ++i) {
        file = files[i];
        console.log("process: " + file);
        var text;
        var secLine;
        var splitSecLine;

        while (true) {
            try {
              text = fs.readFileSync(file, 'utf8');
              secLine = text.split("\n")[1];
              splitSecLine = secLine.split(",");
              fs.unlinkSync(file);
              break; // the program takes a bit to produce the file after end
            }
            catch (e) { }
        }

        res.push(splitSecLine);
    }

    handleCmp(res);

    files = [];
}

function mergeFiles() {
    ended += 1;
    if (ended < concurrent_procs) {
        return;
    }
    var merged_file_name = "merged_cap_tests.txt";
    try {
        fs.unlinkSync(merged_file_name);
        console.log("replaced old merged_cap_tests.txt.");
    } catch (e) {
        console.log("merged_cap_tests.txt did not exist.");
    }

    var res = [];
    for (var i = 0; i < files.length; ++i) {
        file = files[i];
        var text;
        var secLine;
        var splitSecLine;

        while (true) {
            try {
              text = fs.readFileSync(file, 'utf8');
              secLine = text.split("\n")[1];
              splitSecLine = secLine.split(",");
              fs.unlinkSync(file);
              break; // the program takes a bit to produce the file after end
            }
            catch (e) { }
        }

        if (i == 0) {
            fs.writeFileSync(merged_file_name, text, 'utf8');
        } else {
            fs.appendFileSync(merged_file_name, secLine + '\n', 'utf8');
        }
        res.push(splitSecLine);
        if (process.argv[2] == "L1_PADDING") {
            fs.writeFileSync("RESULTS_L1_PADDING", L1_PADDING + "\n", 'utf8');
        } else if (process.argv[2] == "LLC_PADDING") {
            fs.writeFileSync("RESULTS_LLC_PADDING", LLC_PADDING + "\n", 'utf8');
        } else if (process.argv[2] == "BLOOM_FILTER") {
            fs.writeFileSync("RESULTS_BF", BF_M + "\n", 'utf8');
        }
    }

    handleCmp(res);
}
