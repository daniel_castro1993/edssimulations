#ifndef THREADPROCESS_HPP
#define THREADPROCESS_HPP

#include "utils.hpp"

#include <map>
#include <set>

using namespace sim;

class ThreadProcess : public LogicalProcess {
public:
    map<int, ACCESS_T> acquired;
    granule_array_s *L1_cache;
    set<granule_array_s*> L1_used, LLC_used;
    int nb_acqs;
    double per_granule_time;
    int nb_sample;
    int nb_commits, nb_aborts;
    bool ended;

    using LogicalProcess::LogicalProcess;
};

#endif /* THREADPROCESS_HPP */

