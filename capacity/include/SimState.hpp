#ifndef SIMSTATE_HPP
#define SIMSTATE_HPP

#include "utils.hpp"

#include <string>
#include <cstdlib>
#include <cstdio> 

class SimState : public sim::GlobalState {
public:
    int seed, tot_nb_granules, tot_nb_samples, L1_padding;
    int nb_L1_sets, nb_L1_slots, nb_cores;
    double avg_acquisitions, prob_write, prob_evict_read, prob_extra;
    double prob_spurious_r, prob_spurious_w, prob_ab_ev_r, prob_ab_ev_w;
    double bf_m, bf_k;
#ifdef SIMULATE_LLC
    int LLC_padding, LLC_extra_reads, nb_LLC_sets, nb_LLC_slots,;
    granule_array_s **LLC_cache;
#endif
    std::string file_name;
    FILE* file_ptr;
};

#endif /* SIMSTATE_HPP */

