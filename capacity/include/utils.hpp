#ifndef UTILS_HPP
#define UTILS_HPP

#include "EventDrivenSimulator.hpp"

// TODO: move to CMake
#define RANDOM_PADDING

#define UPPER_BOUND_RATIO    100
#define DROP_FIRST_SAMPLES   100

#define new_granules(tp, rand, avg_acq, max_acq) \
        tp->nb_acqs = fmin(max_acq, avg_acq);


//    tp->nb_acqs = fmax(1.0f, rand->ExpDist(1.0 / avg_acq));\
//    if (tp->nb_acqs > max_acq) {\
//        tp->nb_acqs = max_acq;\
//    }

#define INJECT_CONSTRUCTORS(type) \
        type(lpid_s id, simtime_s ts) : EventHandler(id, ts) { }; \
        type(lpid_s id, simtime_s ts, int tags) : EventHandler(id, ts, tags) { };

enum EVENTS_TAGS
{
    E_TAG_ACQUIRE = 0b0001
};

enum ACCESS_T
{
    NONE = 0, WRITE, READ, META
};

struct granule_s
{
    int g;
    ACCESS_T access;
    double timestamp;
};

struct granule_array_s
{
    int count;
    granule_s *array;
};

#endif /* UTILS_HPP */

