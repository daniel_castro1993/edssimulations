#!/bin/bash
cd ./bin
for q in 1 # prob transaction
do
  for p in 1 # prob write
  do
    for t in 2 3 4 5 6 7 8 # threads
    do
      for d in 600 # D
      do
        for l in 10 # L
        do
          for b in 3 # budget
          do
            for c in 1 # time
            do
              ./HTMSim THREADS $t BUDGET $b D $d PW $p L $l PT $q C $c SAMPLES 10000 &
            done
          done
        done
      done
    done
  done
  wait ;
done
