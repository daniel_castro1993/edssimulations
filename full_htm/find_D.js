const fs = require('fs');
const util = require('util');
const os = require('os');
const path = require('path');
const spawn = require('child_process').spawn;
const args = process.argv;

var attempts = 15;
var prev_D = 0, D_HINT, D = 5000, THREADS, BUDGET, C, L, PW, PA, T_COMMIT, T_ABORT, T_INIT, C_SERL;

function recSimSpawn(sim) {
    sim.on('exit', (code) => {
        simSpawn();
    });
}

function simSpawn() {
    
    var prev_res;
    var file = "sim-data.csv";
    
    if (fs.existsSync(file)) {
        prev_res = fs.readFileSync(file, 'utf8');
        var secLine = prev_res.split("\n")[1];
        var parms = secLine.split(",");
        var target_D = parseInt(parms[6]);
        var target_PA = parseFloat(parms[9]);
        console.log("Last D=" + target_D + " with PA=" + target_PA);
        
        if (target_PA > PA) {
            var new_prev_D = D;
            if (prev_D == 0) {
                D *= 2;
            } else {
                D += Math.max(Math.abs(prev_D - D) / 2, 1);
                prev_D = new_prev_D;
            }
        } else {
            var new_prev_D = D;
            if (prev_D == 0) {
                D /= 2;
            } else {
                D -= Math.max(Math.abs(prev_D - D) / 2, 1);
            }
            prev_D = new_prev_D;
        }
        
    } else {
        console.log("first run.");
    }
    
    if (attempts == 0) {
        console.log("Initial D=" + D_HINT + " final D=" + target_D + ".");
        console.log("Computation ended.");
        return null;
    }
    
    attempts -= 1;
    
    var sim = spawn('./HTMSim',
        [
            'SAMPLES', 10000,
            'THREADS', THREADS,
            'BUDGET', BUDGET,
            'CS', C_SERL,
            'CT', C,
            'T_INIT', T_INIT,
            'T_ABORT', T_ABORT,
            'T_COMMIT', T_COMMIT,
            'FILE_METHOD', "w",
            'D', D,
            'PW', PW,
            'L', L,
            'FILE', file
        ], {
            detached: true,
            stdio: 'ignore'
        });
    recSimSpawn(sim);
}

if (args.length != 13) {
    
    console.log("Usage: ");
    console.log("       node find_D.js <THREADS> <BUDGET> <D_HINT> <C> <L> <PW> <PA> <C_SERL> <T_COMMIT> <T_ABORT> <T_INIT>");
    
} else {
    
    THREADS = parseInt(args[2]);
    BUDGET = parseInt(args[3]);
    D = parseFloat(args[4]);
    D_HINT = D;
    C = parseFloat(args[5]);
    L = parseFloat(args[6]);
    PW = parseFloat(args[7]);
    PA = parseFloat(args[8]);
    C_SERL = parseFloat(args[9]);
    T_COMMIT = parseFloat(args[10]);
    T_ABORT = parseFloat(args[11]);
    T_INIT = parseFloat(args[12]);
    
    process.chdir('./bin');
    
    try {
        fs.unlinkSync("sim-data.csv");
        console.log("Unlink success.");
    } catch (e) {
        console.log("File not exist.");
    }
    
    simSpawn();
}
