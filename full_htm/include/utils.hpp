#ifndef UTILS_HPP
#define UTILS_HPP

#include "EventDrivenSimulator.hpp"

#define SIZE_EVENT_T 5
#define UPPER_BOUND_RATIO    1e2
#define BOTTOM_BOUND_RATIO   1e-2
#define DROP_FIRST_SAMPLES   10000

enum EVENTS_TAGS
{
	E_TAG_ACQUIRE = 0b0001
};

enum ACCESS_T
{
	NONE = 0, WRITE = 1, READ = 2
};

enum EVENT_T
{
	TXCM = 0, TXAB, SERL, XACT, NOTX
};

enum CAUSE_T
{
	CONFLICT = 1, LOCK, CAPACITY
};

// TODO: use this function to generate the granules
#define RAND_R_FNC(seed) ({ \
	unsigned long long next = *(seed); \
	unsigned long long result; \
	next *= 1103515245; \
	next += 12345; \
	result = (unsigned long long) (next / 65536) % 2048; \
	next *= 1103515245; \
	next += 12345; \
	result <<= 10; \
	result ^= (unsigned long long) (next / 65536) % 1024; \
	next *= 1103515245; \
	next += 12345; \
	result <<= 10; \
	result ^= (unsigned long long) (next / 65536) % 1024; \
	*(seed) = next; \
	result; \
})

//#define new_bounded_exp(rand, avg) ({ \
//	avg; \
//})

#define get_rand_granule(rand, max) ({ \
	rand->UniformIntDist(1, max); \
})

#define new_bounded_exp(rand, avg) ({\
	double res; \
    res = rand->ExpDist(1.0 / avg); \
    if (res > UPPER_BOUND_RATIO * avg) { \
        res = UPPER_BOUND_RATIO * avg;\
    } else if (res < BOTTOM_BOUND_RATIO * avg) { \
        res = BOTTOM_BOUND_RATIO * avg; \
    } \
	res; \
})

#ifdef TEST_MODE
// this one does not change
#define new_granules(tp, rand, avg_acq, max_acq) \
        tp->nb_acqs = avg_acq;
#else
// future try exp. dist
#define new_granules(tp, rand, avg_acq, max_acq) \
        tp->nb_acqs = avg_acq;

// uncomment for exp dist L
//#define new_granules(tp, rand, avg_acq, max_acq) \
//    tp->nb_acqs = fmax(1.0f, rand->ExpDist(1.0 / avg_acq));\
//    if (tp->nb_acqs > max_acq) {\
//        tp->nb_acqs = max_acq;\
//    }
#endif /* TEST_MODE */


#ifdef TEST_MODE
// this one does not change
#define new_time(tp, rand, avg_compl_time) \
    tp->time = avg_compl_time;
#else
// exp. dist
#define new_time(tp, rand, avg_compl_time) \
    tp->time = new_bounded_exp(rand, avg_compl_time);
#endif /* TEST_MODE */

#define INJECT_CONSTRUCTORS(type)\
	type(lpid_s id, simtime_s ts) : EventHandler(id, ts) { };\
	type(lpid_s id, simtime_s ts, int tags) : EventHandler(id, ts, tags) { };

#define mc_state_size(budget) ({ \
    budget + 2; \
})

#define mc_state_set(vec, budget_per_thr, threads, budget) ({ \
    int i; \
    vec.clear(); \
    vec.resize(mc_state_size(budget), 0); \
    for (i = 0; i < threads; ++i) { \
        if (budget_per_thr[i] > 0) { \
            vec[budget - budget_per_thr[i]]++; \
        } else { \
            vec[budget + 1]++; \
        } \
    } \
})

#ifndef DISABLE_STATES_DBG
#define mc_state_update(tp, st, lat) ({ \
    vector<int> mc_state(mc_state_size(st->budget)); \
    int count_writes = st->CountWrites(); \
    st->budget_lps[tp->GetId()] = tp->budget_remaining; \
    mc_state_set(mc_state, st->budget_lps, st->nb_lps, st->budget); \
    if (st->mc_states.find(mc_state) == st->mc_states.end()) { \
            pair<int, int> new_val = make_pair(1, count_writes); \
            st->mc_states[mc_state] = lat; \
            st->mc_states_writes[mc_state] = new_val; \
    } else { \
            st->mc_states[mc_state] += lat; \
            st->mc_states_writes[mc_state].first += 1; \
            st->mc_states_writes[mc_state].second += count_writes; \
    } \
})
#else
#define mc_state_update(tp, st, lat) ({})
#endif /* DISABLE_STATES_DBG */

#define mc_state_print(fptr, vec, budget) ({ \
    int i; \
    fprintf(fptr, "["); \
    for (i = 0; i < mc_state_size(budget); ++i) { \
        fprintf(fptr, "%i", vec[i]); \
        if (i != mc_state_size(budget) - 1) { \
            fprintf(fptr, ","); \
        } \
    } \
    fprintf(fptr, "]"); \
})

#endif /* UTILS_HPP */

