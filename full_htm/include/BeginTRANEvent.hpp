#ifndef BEGINTRANEVENT_HPP
#define BEGINTRANEVENT_HPP

#include "utils.hpp"

using namespace sim;

class BeginTRANEvent : public EventHandler
{
public:
	CAUSE_T cause;
	
	INJECT_CONSTRUCTORS(BeginTRANEvent)
	
	void Execute(LogicalProcess*, GlobalState*);
};

#endif /* BEGINTRANEVENT_HPP */

