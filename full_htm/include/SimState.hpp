#ifndef SIMSTATE_HPP
#define SIMSTATE_HPP

#include "utils.hpp"

#include <string>
#include <cstdlib>
#include <cstdio> 
#include <queue>
#include <vector>
#include <map>

using namespace sim;
using namespace std;

class SimState : public GlobalState
{
public:
    int seed, nb_lps, pool_size, tot_nb_samples, budget;
    double avg_acqs, prob_write, prob_t;
    simtime_s avg_time, avg_time_serl, avg_time_notx;
    simtime_s t_commit, t_abort, t_init;
    string file_name, file_method;
    FILE *file_ptr;
#ifdef USE_LOG
    FILE *log_ptr;
#endif /* USE_LOG */
    bool lock_taken;
    queue<lpid_s> blocked;
    map<lpid_s, bool> am_blocked;
    map<int, map<lpid_s, ACCESS_T>> accessed;
    map<vector<int>, double> mc_states;
    map<vector<int>, pair<int, int>> mc_states_writes;
    vector<int> budget_lps;
    simtime_s last_state_ts;

    void CleanUpAccessed(lpid_s, map<int, ACCESS_T>&);
    int CountWrites();
    void TopStates(vector<tuple<vector<int>, double, double>>&);
};

#endif /* SIMSTATE_HPP */

