#ifndef SERIALCOMMITEVENT_HPP
#define SERIALCOMMITEVENT_HPP

#include "utils.hpp"

using namespace sim;

class SerialCommitEvent : public EventHandler {
public:
	INJECT_CONSTRUCTORS(SerialCommitEvent)

	void Execute(LogicalProcess*, GlobalState*);
} ;

#endif /* SERIALCOMMITEVENT_HPP */

