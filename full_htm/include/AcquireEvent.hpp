#ifndef ACQUIREEVENT_HPP
#define ACQUIREEVENT_HPP

#include "utils.hpp"

using namespace sim;

class AcquireEvent : public EventHandler
{
public:
	int g;
	ACCESS_T access;

	INJECT_CONSTRUCTORS(AcquireEvent)

	void Execute(LogicalProcess*, GlobalState*);
};

#endif /* ACQUIREEVENT_HPP */

