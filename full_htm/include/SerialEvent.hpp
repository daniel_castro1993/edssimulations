#ifndef SERIALEVENT_HPP
#define SERIALEVENT_HPP

#include "utils.hpp"

using namespace sim;
using namespace std;

class SerialEvent : public EventHandler
{
public:
	INJECT_CONSTRUCTORS(SerialEvent)
	
	void Execute(LogicalProcess*, GlobalState*);
};

#endif /* SERIALEVENT_HPP */

