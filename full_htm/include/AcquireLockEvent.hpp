#ifndef ACQUIRELOCKEVENT_HPP
#define ACQUIRELOCKEVENT_HPP

#include "utils.hpp"

using namespace sim;

class AcquireLockEvent : public EventHandler
{
public:
	CAUSE_T cause;
	
	INJECT_CONSTRUCTORS(AcquireLockEvent)
	
	void Execute(LogicalProcess*, GlobalState*);
};

#endif /* ACQUIRELOCKEVENT_HPP */

