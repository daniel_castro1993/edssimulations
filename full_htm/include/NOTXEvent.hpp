#ifndef NOTXEVENT_HPP
#define NOTXEVENT_HPP

#include "utils.hpp"

using namespace sim;
using namespace std;

class NOTXEvent : public EventHandler {
public:
	INJECT_CONSTRUCTORS(NOTXEvent)
	
	void Execute(LogicalProcess*, GlobalState*);
} ;

#endif /* NOTXEVENT_HPP */

