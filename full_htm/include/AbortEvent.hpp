#ifndef ABORTEVENT_HPP
#define ABORTEVENT_HPP

#include "utils.hpp"

using namespace sim;

class AbortEvent : public EventHandler
{
public:
	CAUSE_T cause;
	
	INJECT_CONSTRUCTORS(AbortEvent)
	
	void Execute(LogicalProcess*, GlobalState*);
};

#endif /* ABORTEVENT_HPP */

