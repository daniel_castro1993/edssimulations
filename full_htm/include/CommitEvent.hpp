#ifndef COMMITEVENT_HPP
#define COMMITEVENT_HPP

#include "utils.hpp"

using namespace sim;

class CommitEvent : public EventHandler
{
public:
	
	INJECT_CONSTRUCTORS(CommitEvent)
	
	void Execute(LogicalProcess*, GlobalState*);

};

#endif /* COMMITEVENT_HPP */

