#ifndef THREADPROCESS_HPP
#define THREADPROCESS_HPP

#include "utils.hpp"

using namespace sim;

class SimState;

class ThreadProcess : public LogicalProcess
{
public:
	map<int, ACCESS_T> acquired;
	int nb_acqs;
	simtime_s time, ts_start, ts_last_abort, ts_init;
	int nb_sample, budget_remaining, count_retries;
	avg_s retries;
	simtime_s times[SIZE_EVENT_T];
	unsigned long long count[SIZE_EVENT_T];
	unsigned long long count_accesses_on_abort;
#ifdef USE_RAND_FNC
	unsigned long long seed;
#endif /* USE_RAND_FNC */
	SimState *simState;

	using LogicalProcess::LogicalProcess;

	virtual void CustomReset();
};

#endif /* THREADPROCESS_HPP */

