#ifndef INITEVENT_HPP
#define INITEVENT_HPP

#include "utils.hpp"

using namespace sim;

class InitEvent : public EventHandler
{
public:
	INJECT_CONSTRUCTORS(InitEvent)

	void Execute(LogicalProcess*, GlobalState*);
};


#endif /* INITEVENT_HPP */

