#!/bin/bash
cd ./bin
for q in 0.9 0.5 0.1 # prob transaction
do
  for p in 1 0.9 0.5 0.1 # prob write
  do
    for d in 1024 2048 8192 # D
    do
      for l in 2 5 10 15 20 25 30 # L
      do
        for t in 2 3 4 # threads
        do
          for b in 3 4 5 # budget
          do
            for c in 1 4 8 # time
            do
              ./HTMSim SAMPLES 5000 THREADS $t BUDGET $b D $d PW $p L $l PT $q CT $c CN $c CS $c &
            done
          done
        done
        wait ;
      done
    done
  done
  wait ;
done

for q in 0.9 0.5 0.1 # prob transaction
do
  for p in 1 0.9 0.5 0.1 # prob write
  do
    for d in 1024 2048 8192 # D
    do
      for l in 2 5 10 15 20 # L
      do
        for t in 6 8 # threads
        do
          for b in 3 # budget
          do
            for c in 1 4 8 # time
            do
              ./HTMSim SAMPLES 5000 THREADS $t BUDGET $b D $d PW $p L $l PT $q CT $c CN $c CS $c &
            done
          done
        done
        wait ;
      done
    done
  done
  wait ;
done
