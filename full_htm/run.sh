#!/bin/bash
cd ./bin
for q in 1 # prob transaction
do
  for p in 1 # prob write
  do
    for t in 100 200 300 400 500 600 700 800 900 1000 # threads
    do
      for d in $((250 * $t + 10000)) # D
      do
        for l in 10 # L
        do
          for b in 5 # budget
          do
            for c in 1 # time
            do
              ./model THREADS $t BUDGET $b D $d PW $p L $l P_ST $q P_SN $q C $c &
            done
          done
        done
      done
    done
  done
  wait ;
done
