#include "app.hpp"

static void Intel_check_conflicts(ThreadProcess*, SimState*,
								  simtime_s, int, ACCESS_T);
static void P8_check_conflicts(ThreadProcess*, SimState*,
							   simtime_s, int, ACCESS_T);

/*
 * Story of the transaction:
 *   1) waits C/L
 *   2) start and gets right away the granule
 *   3) waits C/L
 *   4) acquires other granule
 *    --> if was the last granule triggers other AcquireEvent with lat=0
 */

void AcquireEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess* tp = dynamic_cast<ThreadProcess*> (lp);
	SimState* st = dynamic_cast<SimState*> (state);

	RandomLib *r = RandomLib::getInstance(0);
	simtime_s lat = tp->time / tp->nb_acqs;
	simtime_s next_ts;
	AcquireEvent *acqEV;
	CommitEvent *commitEV;
	AbortEvent *abortEV;
	int next_g;
	ACCESS_T next_access;

	if (st->lock_taken) {
		// LOCK TAKEN -> BLOCK

		abortEV = NEW_EVENT(AbortEvent,
							lp->GetId(),
							next_ts,
							0);

		schedule_event(lp->GetId(), abortEV);
	}
	else if (tp->acquired.size() < tp->nb_acqs) {
		// ACCESS

#ifdef USE_LOG
		fprintf(st->log_ptr, "%f,%i,ACQUIRE g=%i ACCESS=%c\n", now,
				tp->GetId(), g, access == WRITE ? 'W' : 'R');
#endif /* USE_LOG */

		next_access = r->BernoulliDist(st->prob_write) ? WRITE : READ;
		tp->acquired[g] = access; // this g, not the next one

#ifdef USE_RAND_FNC
		int timeout = 100;
#endif /* USE_RAND_FNC */

		do {
#ifdef TEST_MODE
			next_g = tp->acquired.size();
#elif USE_RAND_FNC
			unsigned long long r_tmp;
			unsigned long long r_res;
			r_res = RAND_R_FNC(&(tp->seed));
			next_g = r_res % st->pool_size;
			//			tp->seed ^= garbage2;
			r_res = RAND_R_FNC(&(tp->seed));
			next_access = (double) (r_res % 0xFFFFF) / (double) (0xFFFFF - 1)
					< st->prob_write ? WRITE : READ;
#else
			next_g = get_rand_granule(r, st->pool_size);
#endif /* TEST_MODE && USE_RAND_FNC */
		}
		while (tp->acquired.find(next_g) != tp->acquired.end()
#ifdef USE_RAND_FNC
				&& --timeout > 0
#endif /* USE_RAND_FNC */
				);

#ifdef USE_P8
		P8_check_conflicts(tp, st, now, g, access);
#else
		Intel_check_conflicts(tp, st, now, g, access);
#endif /* USE_P8 */

		if (tp->acquired.size() < tp->nb_acqs) {
			next_ts = NEXT_TIMESTAMP(now + lat);
		}
		else {
			next_ts = NEXT_TIMESTAMP(now);
		}

		acqEV = NEW_EVENT(AcquireEvent,
						lp->GetId(),
						next_ts,
						E_TAG_ACQUIRE);

		acqEV->g = next_g;
		acqEV->access = next_access;

		schedule_event(lp->GetId(), acqEV);
	}
	else {
		// COMMIT

		commitEV = NEW_EVENT(CommitEvent,
							lp->GetId(),
							NEXT_TIMESTAMP(now + st->t_commit),
							0);
		schedule_event(lp->GetId(), commitEV);
	}
}

static void Intel_check_conflicts(ThreadProcess *tp,
								  SimState *st,
								  simtime_s now,
								  int g,
								  ACCESS_T access)
{
	map<int, map<lpid_s, ACCESS_T> >::iterator find;
	AbortEvent *abortEV;

	find = st->accessed.find(g);

	if (find != st->accessed.end()) {
		map<lpid_s, ACCESS_T>::iterator it,
				begin = find->second.begin(),
				end = find->second.end();

		for (it = begin; it != end; ++it) {
			if (it->first != tp->GetId() &&
				(it->second == WRITE || access == WRITE)) {

				//ABORT 

				abortEV = NEW_EVENT(AbortEvent,
									tp->GetId(),
									NEXT_TIMESTAMP(now),
									0);

				schedule_event(it->first, abortEV);
				return;
			}
		}
	}
	st->accessed[g][tp->GetId()] = access;
}

static void P8_check_conflicts(ThreadProcess *tp,
							   SimState *st,
							   simtime_s now,
							   int g,
							   ACCESS_T access)
{
	map<int, map<lpid_s, ACCESS_T> >::iterator find;
	AbortEvent *abortEV;

	find = st->accessed.find(g);

	if (find != st->accessed.end()) {
		map<lpid_s, ACCESS_T>::iterator it,
				begin = find->second.begin(),
				end = find->second.end();

		for (it = begin; it != end; ++it) {
			if (it->first != tp->GetId() &&
				(it->second == WRITE)) {

				// I abort because there is a older write

				abortEV = NEW_EVENT(AbortEvent,
									tp->GetId(),
									NEXT_TIMESTAMP(now),
									0);

				schedule_event(tp->GetId(), abortEV);
				return;
			}
			else if (it->first != tp->GetId() &&
					 (it->second == READ && access == WRITE)) {

				// The other abort 

				abortEV = NEW_EVENT(AbortEvent,
									tp->GetId(),
									NEXT_TIMESTAMP(now),
									0);

				schedule_event(it->first, abortEV);
			}
		}
	}
	st->accessed[g][tp->GetId()] = access;
}
