#include "app.hpp"

void BeginTRANEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess* tp = dynamic_cast<ThreadProcess*> (lp);
	SimState* st = dynamic_cast<SimState*> (state);
	simtime_s next_ts = NEXT_TIMESTAMP(now);
	RandomLib *r = RandomLib::getInstance(0);
	AcquireEvent *acqEV;
	SerialEvent *serlEV;
	int g;
	ACCESS_T access;

	tp->ts_last_abort = next_ts;

	st->budget_lps[lp->GetId()] = tp->budget_remaining;

	if (st->lock_taken && !st->am_blocked[tp->GetId()]) {
		st->am_blocked[tp->GetId()] = true;
		st->blocked.push(lp->GetId());
	}
	else if (tp->budget_remaining <= 0) {
		serlEV = NEW_EVENT(SerialEvent, lp->GetId(), next_ts, 0);
		schedule_event(lp->GetId(), serlEV);
	}
	else {
		new_granules(tp, r, st->avg_acqs, st->pool_size);
		new_time(tp, r, st->avg_time);

		simtime_s lat = tp->time / tp->nb_acqs;
		next_ts = NEXT_TIMESTAMP(next_ts + lat);

		tp->ts_last_abort = next_ts;

		acqEV = NEW_EVENT(AcquireEvent, lp->GetId(), next_ts, E_TAG_ACQUIRE);

		g = get_rand_granule(r, st->pool_size);
		access = r->BernoulliDist(st->prob_write) ? WRITE : READ;

#ifdef TEST_MODE
		acqEV->g = tp->acquired.size();
#elif USE_RAND_FNC
		unsigned long long r_tmp;
		unsigned long long r_res;
		r_res = RAND_R_FNC(&(tp->seed));
		acqEV->g = r_res % st->pool_size;
		access = (double) (r_res % 0xFFFFF) / (double) 0xFFFFF
				< st->prob_write ? WRITE : READ;
#else
		acqEV->g = g;
#endif /* TEST_MODE && USE_RAND_FNC */

		acqEV->access = access;

		schedule_event(lp->GetId(), acqEV);
	}
}

