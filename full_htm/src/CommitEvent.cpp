#include "app.hpp"

void CommitEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess* tp = dynamic_cast<ThreadProcess*> (lp);
	SimState* st = dynamic_cast<SimState*> (state);
	InitEvent *initEV;
	simtime_s next_ts = NEXT_TIMESTAMP(now), lat = now - tp->ts_last_abort;

#ifdef USE_LOG
	fprintf(st->log_ptr, "%f,%i,COMMIT\n", now, tp->GetId());
#endif /* USE_LOG */

	tp->retries += tp->count_retries;

	st->CleanUpAccessed(lp->GetId(), tp->acquired);
	tp->acquired.clear();

	tp->count[TXCM]++;
	tp->times[TXCM] += lat;
	tp->count[XACT]++;
	tp->times[XACT] += now - tp->ts_start;
	mc_state_update(tp, st, lat);

	initEV = NEW_EVENT(InitEvent, lp->GetId(), next_ts, 0);
	schedule_event(lp->GetId(), initEV);
}