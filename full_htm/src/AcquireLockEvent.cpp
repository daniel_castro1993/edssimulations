#include "app.hpp"

void AcquireLockEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
	SimState *st = dynamic_cast<SimState*> (state);
	simtime_s next_ts = NEXT_TIMESTAMP(now);
	AbortEvent *abortEv;

	// TODO: sometimes st->blocked.size() != 0, I believe is due to after the
	// SERL_COMMIT event, threads are scheduled to the same time (the first
	// grabs the lock and the others block)
	if (tp->budget_remaining == -100 || lp->GetId() == scheduler_process
		|| st->am_blocked[lp->GetId()]) {
		// NOTX: does not matter
		return;
	}

	abortEv = NEW_EVENT(AbortEvent, lp->GetId(), next_ts, 0);
	schedule_event(lp->GetId(), abortEv);
}
