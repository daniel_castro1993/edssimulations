#include "app.hpp"

using namespace sim;

void ThreadProcess::CustomReset()
{
	times[SERL] = 0;
	times[TXCM] = 0;
	times[TXAB] = 0;
	times[NOTX] = 0;
	times[XACT] = 0;
	count[SERL] = 0;
	count[TXCM] = 0;
	count[TXAB] = 0;
	count[NOTX] = 0;
	count[XACT] = 0;
	count_accesses_on_abort = 0;

	if (simState->mc_states.size() > 0) {
		simState->mc_states.clear();
		simState->mc_states_writes.clear();
	}

	retries.count = 0;
	nb_sample = 0;
	ts_init = GetCurrentTime();
}
