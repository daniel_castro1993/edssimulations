#include "app.hpp"

#include <cstdlib>
#include <cstdio> 
#include <ctime>
#include <iostream>

using namespace sim;
using namespace std;

extern int reset_threshold;
extern int end_threshold;

// entry point for the simulation

void init(map<string, string>& args) {
    ThreadProcess *tLP;
    InitEvent *initEV;
    SimState *state = new SimState();
    int i;
    lpid_s lpid;

    SET_STATE(SimState, state);
    reset_threshold = DROP_FIRST_SAMPLES;

    state->seed = clock();
    state->nb_lps = 10;
    state->pool_size = 1000;
    state->tot_nb_samples = 10;
    state->avg_acqs = 10;
    state->avg_time = 1;
    state->avg_time_serl = 1;
    state->avg_time_notx = 1;
    state->prob_write = 1.0;
    state->prob_t = 1.0;
    state->budget = 5;
    state->file_name = "conflits.txt";
    state->lock_taken = false;
    state->t_commit = 0;
    state->t_abort = 0;
    state->t_init = 0;
    state->file_method = "a+";

#ifdef USE_LOG
    state->log_ptr = fopen("LOG.txt", "w");
    fprintf(state->log_ptr, "TIME,THREAD,INFO\n");
#endif /* USE_LOG */ 

    if (!args["DEFAULT_SEED"].empty()) {
        state->seed = stoi(args["DEFAULT_SEED"]);
    }

    if (!args["THREADS"].empty()) {
        state->nb_lps = stoi(args["THREADS"]);
    }

    if (!args["BUDGET"].empty()) {
        state->budget = stoi(args["BUDGET"]);
    }

    if (!args["PW"].empty()) {
        state->prob_write = stod(args["PW"]);
    }

    if (!args["PT"].empty()) {
        state->prob_t = stod(args["PT"]);
    }

    if (!args["D"].empty()) {
        state->pool_size = stoi(args["D"]);
    }

    if (!args["CT"].empty()) {
        state->avg_time = stod(args["CT"]);
    }

    if (!args["CN"].empty()) {
        state->avg_time_notx = stod(args["CN"]);
    }

    if (!args["CS"].empty()) {
        state->avg_time_serl = stod(args["CS"]);
    }

    if (!args["L"].empty()) {
        state->avg_acqs = stod(args["L"]);
    }

    if (!args["T_COMMIT"].empty()) {
        state->t_commit = stod(args["T_COMMIT"]);
    }

    if (!args["T_ABORT"].empty()) {
        state->t_abort = stod(args["T_ABORT"]);
    }

    if (!args["T_INIT"].empty()) {
        state->t_init = stod(args["T_INIT"]);
    }

    if (!args["SAMPLES"].empty()) {
        state->tot_nb_samples = stoi(args["SAMPLES"]);
    }

    if (!args["FILE"].empty()) {
        state->file_name = args["FILE"];
    }

    if (!args["FILE_METHOD"].empty()) {
        state->file_method = args["FILE_METHOD"];
    }

    if (state->pool_size < state->avg_acqs) {
        printf("POOL_SIZE must be greater than AVG_ACQS\n");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < state->nb_lps; ++i) {
        state->budget_lps.push_back(-1);
    }

    end_threshold = state->tot_nb_samples * state->avg_acqs * state->nb_lps;

    printf("Started simulation with parameters: \n"
        "DEFAULT_SEED = %i\n"
        "SAMPLES = %i\n"
        "THREADS = %i\n"
        "BUDGET = %i\n"
        "P_W = %.2f\n"
        "P_T = %.2f\n"
        "D = %i\n"
        "CT = %f\n"
        "CS = %f\n"
        "CN = %f\n"
        "L = %f\n"
        "T_INIT = %f\n"
        "T_COMMIT = %f\n"
        "T_ABORT = %f\n",
        state->seed, state->tot_nb_samples, state->nb_lps, state->budget,
        state->prob_write, state->prob_t, state->pool_size, state->avg_time,
        state->avg_time_serl, state->avg_time_notx, state->avg_acqs,
        state->t_init, state->t_commit, state->t_abort);

#ifdef USE_RAND_FNC
    srand(time(NULL));
    unsigned long long same_seed = (unsigned long long) rand();
#endif /* USE_RAND_FNC */

    RandomLib::create(state->seed);
    for (i = 0; i < state->nb_lps; ++i) {
        tLP = new ThreadProcess();
        tLP->simState = state;
#ifdef USE_RAND_FNC
        tLP->seed = (unsigned long long) rand();
#endif /* USE_RAND_FNC */
        lpid = add_logical_process(tLP);
        initEV = NEW_EVENT(InitEvent, lpid, NEXT_TIMESTAMP(0), 0);
        schedule_event(lpid, initEV);
        tLP->CustomReset();
    }
}

void end(vector<LogicalProcess*> lps, GlobalState *state) {
    vector<LogicalProcess*>::iterator it,
        begin = lps.begin(),
        end = lps.end();
    int i;
    int tot_lps = lps.size(), tot_commits = 0, tot_aborts = 0, tot_serls = 0;
    double tot_avg_retries = 0, prob_abort, prob_serl, throughput, r_xact,
        time_serl = 0, time_notx = 0, time_txab = 0, time_txcm = 0,
        tot_time = 0, avg_TXCM_time, avg_TXAB_time, time_xact,
        tot_xact = 0, avg_acq_abort = 0;

    SimState *st = dynamic_cast<SimState*> (state);

    st->file_ptr = fopen(st->file_name.c_str(), st->file_method.c_str());
    for (it = begin; it != end; ++it) {
        ThreadProcess* tp = dynamic_cast<ThreadProcess*> (*it);
        tot_avg_retries += tp->retries.avg;
        tot_commits += tp->count[TXCM];
        tot_aborts += tp->count[TXAB];
        tot_serls += tp->count[SERL];
        tot_xact += tp->count[XACT];
        tot_time += tp->GetCurrentTime() - tp->ts_init;

        printf("[LP %i] commits: %i, aborts: %i (samples: %i)\n",
            tp->GetId(), tp->count[TXCM], tp->count[TXAB], tp->nb_sample);

        time_serl += tp->times[SERL];
        time_notx += tp->times[NOTX];
        time_txab += tp->times[TXAB];
        time_txcm += tp->times[TXCM];
        time_xact += tp->times[XACT];

        avg_acq_abort += tp->count_accesses_on_abort;
    }

    avg_TXCM_time = time_txcm / tot_commits;
    avg_TXAB_time = time_txab / tot_aborts;
    avg_acq_abort /= tot_aborts;

    //	tot_time = time_serl + time_notx + time_txab + time_txcm;

    prob_abort = (double) tot_aborts / (double) (tot_aborts + tot_commits);
    prob_serl = (double) tot_serls / (double) (tot_commits + tot_serls);
    tot_avg_retries /= (double) tot_lps;
    r_xact = time_xact / tot_xact;
    throughput = (tot_commits + tot_serls) / (tot_time / (double) tot_lps);
    //	throughput = tot_lps / (r_xact); // test against real hardware
    fseek(st->file_ptr, 0, SEEK_END);
    if (ftell(st->file_ptr) < 8) {
        fprintf(st->file_ptr, "%12s,%12s,%12s,%12s,%12s,%12s,%12s,%12s,"
            "%12s,%12s,%12s,%12s,%12s\n", "SEED", "SAMPLES", "THREADS",
            "BUDGET", "P_T", "P_W", "D", "C", "L", "P_A", "P_serl",
            "R_XACT", "Throughput");
    }

    fprintf(st->file_ptr, "%12i,%12i,%12i,%12i,%12.3f,%12.3f,%12i,%12f,"
        "%12f,%12f,%12f,%12f,%12f\n", st->seed, st->tot_nb_samples,
        st->nb_lps, st->budget, st->prob_t, st->prob_write, st->pool_size,
        st->avg_time, st->avg_acqs, prob_abort, prob_serl, r_xact,
        throughput);
    fclose(st->file_ptr);

    printf("\n Global:\n");
    printf("TXCM_t: %.2f\n", avg_TXCM_time);
    printf("TXAB_t: %.2f\n", avg_TXAB_time);
    printf("AVG_RT: %.2f\n", tot_avg_retries);
    printf("P_a   : %.2f%%\n", prob_abort * 100.0);
    printf("P_serl: %.2f%%\n", prob_serl * 100.0);
    printf("X     : %f\n", throughput);

#ifndef DISABLE_STATES_DBG
    vector<tuple < vector<int>, double, double>> mc_states;

    st->TopStates(mc_states);

    printf("\n\nTop 10 states: \n");

    for (i = 0; i < 10; ++i) {
        if (mc_states.size() <= i) {
            break;
        }
        vector<int> state;
        double freq;
        double writes;

        tie(state, freq, writes) = mc_states[i];

        mc_state_print(stdout, state, st->budget);
        printf(": %.3f%% (writes: %.3f)\n", freq * 100.0, writes);
    }
#endif /* DISABLE_STATES_DBG */

#ifdef USE_LOG
    fclose(st->log_ptr);
#endif /* USE_LOG */
}
