#include "app.hpp"

#define RANDOM_LAT 5 * st->nb_lps

void SerialCommitEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
	SimState *st = dynamic_cast<SimState*> (state);
	RandomLib *r = RandomLib::getInstance(0);
	simtime_s next_ts = NEXT_TIMESTAMP(now), lat = now - tp->ts_last_abort;
	InitEvent *initEv;
	BeginTRANEvent *begEv;

	assert(st->blocked.size() < st->nb_lps);
	
	mc_state_update(tp, st, lat);

	// restart normal operation
	initEv = NEW_EVENT(InitEvent, lp->GetId(), next_ts, 0);
	schedule_event(lp->GetId(), initEv);

	tp->count[SERL]++;
	tp->times[SERL] += lat;
	tp->count[XACT]++;
	tp->times[XACT] += now - tp->ts_start;

#ifdef USE_LOG
	fprintf(st->log_ptr, "%f,%i,SERL_COMMIT\n", now, tp->GetId());
#endif /* USE_LOG */

	st->lock_taken = false;
	while (!st->blocked.empty()) {
		lpid_s id = st->blocked.front();
		simtime_s next_rand_ts = NEXT_TIMESTAMP(next_ts);
		int rand_int = r->UniformIntDist(0, RANDOM_LAT);
		int i;

		// This extra latency ensures threads are uniformly
		for (i = 0; i < rand_int; ++i) {
			next_rand_ts = NEXT_TIMESTAMP(next_rand_ts);
		}

		begEv = NEW_EVENT(BeginTRANEvent, lp->GetId(), next_rand_ts, 0);
		schedule_event(id, begEv);

		st->am_blocked[id] = false;
		st->blocked.pop();
	}
}

