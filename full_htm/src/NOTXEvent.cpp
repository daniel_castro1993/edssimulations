#include "app.hpp"

void NOTXEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
	SimState *st = dynamic_cast<SimState*> (state);
	RandomLib *r = RandomLib::getInstance(0);
	simtime_s lat, next_ts;
	InitEvent *initEv;

	new_granules(tp, r, st->avg_acqs, st->pool_size);
	new_time(tp, r, st->avg_time_notx);

	lat = tp->time;
	next_ts = now + lat;
	next_ts = NEXT_TIMESTAMP(next_ts);

	tp->count[NOTX]++;
	tp->times[NOTX] += lat;

	initEv = NEW_EVENT(InitEvent, lp->GetId(), next_ts, 0);
	schedule_event(lp->GetId(), initEv);
}
