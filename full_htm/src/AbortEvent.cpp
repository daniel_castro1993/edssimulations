#include "app.hpp"

void AbortEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
	SimState *st = dynamic_cast<SimState*> (state);
	RandomLib *r = RandomLib::getInstance(0);
	simtime_s time_abort = st->t_abort > 0 ?
			new_bounded_exp(r, st->t_abort) : 0;
	simtime_s next_ts = NEXT_TIMESTAMP(now + time_abort),
			lat = now - tp->ts_last_abort;
	BeginTRANEvent *begEV;

	//#ifdef TEST_MODE
	//	next_ts -= lat;
	//	next_ts += st->avg_time; // serializes transactions
	//#endif /* TEST_MODE */

	unschedule_all_events(lp->GetId());

	if (tp->acquired.size() > 0) {
		// actually aborts

#ifdef USE_LOG
		fprintf(st->log_ptr, "%f,%i,ABORT\n", now, tp->GetId());
#endif /* USE_LOG */

		tp->count[TXAB]++;
		tp->times[TXAB] += lat;
		
		tp->count_accesses_on_abort += tp->acquired.size();

		tp->count_retries++;
		if (tp->budget_remaining > 0) {
			mc_state_update(tp, st, lat);
			tp->budget_remaining--; // updates only after
		}

		st->CleanUpAccessed(lp->GetId(), tp->acquired);
		tp->acquired.clear();
	}

	if (st->lock_taken) {
		// LOCK TAKEN -> BLOCK

		if (!st->am_blocked[tp->GetId()]) {
			st->am_blocked[tp->GetId()] = true;
			st->blocked.push(lp->GetId());
		}
	}
	else {
		begEV = NEW_EVENT(BeginTRANEvent, lp->GetId(), next_ts, 0);
		schedule_event(lp->GetId(), begEV);
	}
}
