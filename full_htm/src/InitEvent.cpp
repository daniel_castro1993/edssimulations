#include "app.hpp"
#include "NOTXEvent.hpp"

void InitEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
	SimState *st = dynamic_cast<SimState*> (state);
	RandomLib *r = RandomLib::getInstance(0);
	simtime_s time_init = st->t_init > 0 ?
			new_bounded_exp(r, st->t_init) : 0;
	simtime_s next_ts;
	BeginTRANEvent *begEV;
	NOTXEvent *notxEV;

	if (now == NEXT_TIMESTAMP(0)) {
		next_ts = NEXT_TIMESTAMP(now);
	} else {
		next_ts = NEXT_TIMESTAMP(now + time_init);
	}
	
	tp->nb_sample++;
	st->am_blocked[tp->GetId()] = false;

	new_granules(tp, r, st->avg_acqs, st->pool_size);
	new_time(tp, r, st->avg_time);

	tp->ts_start = next_ts;

	if (r->BernoulliDist(st->prob_t)) {

		tp->budget_remaining = st->budget;
		tp->count_retries = 0;

		st->budget_lps[lp->GetId()] = tp->budget_remaining ;

		begEV = NEW_EVENT(BeginTRANEvent, lp->GetId(), next_ts, 0);
		schedule_event(lp->GetId(), begEV);
	}
	else {

		tp->budget_remaining = -100;

		notxEV = NEW_EVENT(NOTXEvent, lp->GetId(), next_ts, E_TAG_ACQUIRE);
		schedule_event(lp->GetId(), notxEV);
	}
}


