#include "app.hpp"

void SerialEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
	SimState *st = dynamic_cast<SimState*> (state);
	RandomLib *r = RandomLib::getInstance(0);
	simtime_s end_ts = now, next_ts = NEXT_TIMESTAMP(now);
	SerialCommitEvent *commitEv;
	AcquireLockEvent *lockEV;

	new_granules(tp, r, st->avg_acqs, st->pool_size);
	new_time(tp, r, st->avg_time_serl);

	end_ts += tp->time;
	end_ts = NEXT_TIMESTAMP(end_ts);

	if (!st->lock_taken) {
		st->lock_taken = true;

#ifdef USE_LOG
		fprintf(st->log_ptr, "%f,%i,SERL\n", now, tp->GetId());
#endif /* USE_LOG */		
		
		commitEv = NEW_EVENT(SerialCommitEvent, lp->GetId(), end_ts, 0);
		schedule_event(lp->GetId(), commitEv);
		
		lockEV = NEW_EVENT(AcquireLockEvent, lp->GetId(), next_ts, 0);
		broadcast_event(lockEV);
	}
	else if (!st->am_blocked[tp->GetId()]) {
		// else wait (not schedule nothing)
		st->am_blocked[tp->GetId()] = true;
		st->blocked.push(lp->GetId());
	}
}
