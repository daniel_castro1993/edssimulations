#include "SimState.hpp"

#include <algorithm>

void SimState::CleanUpAccessed(lpid_s id, map<int, ACCESS_T> &acquired)
{
	map<int, ACCESS_T>::iterator it,
			begin = acquired.begin(),
			end = acquired.end();

	for (it = begin; it != end; ++it) {
		int g = it->first;
		map<int, map<lpid_s, ACCESS_T> >::iterator find;
		map<lpid_s, ACCESS_T>::iterator find_me;

		find = accessed.find(g);
		if (find != accessed.end()) {
			find_me = find->second.find(id);
			if (find_me != find->second.end()) {
				find->second.erase(find_me);
			}

			if (find->second.empty()) {
				accessed.erase(find);
			}
		}
	}
}

int SimState::CountWrites()
{
	int res = 0;
	map<int, map<lpid_s, ACCESS_T>>::iterator it,
			begin = accessed.begin(),
			end = accessed.end();

	for (it = begin; it != end; ++it) {
		map<lpid_s, ACCESS_T>::iterator it2,
				begin2 = it->second.begin(),
				end2 = it->second.end();

		for (it2 = begin2; it2 != end2; ++it2) {
			if (it2->second == WRITE) {
				res += 1;
			}
		}
	}

	return res;
}

void SimState::TopStates(vector<tuple<vector<int>, double, double>> &vec)
{
	double count = 0.0;
	map<vector<int>, double>::iterator it,
			begin = mc_states.begin(),
			end = mc_states.end();

	vec.clear();

	for (it = begin; it != end; ++it) {
		double lat_in_state = it->second;
		count += lat_in_state;
	}
	
	for (it = begin; it != end; ++it) {
		int count_times = mc_states_writes[it->first].first;
		int count_writes = mc_states_writes[it->first].second;
		double avg_writes = (double) count_writes / (double) count_times;
		double lat_in_state = it->second / count; // normalize
		tuple<vector<int>, double, double> item =
				make_tuple(it->first, lat_in_state, avg_writes);
		vec.push_back(item);
	}

	sort(vec.begin(), vec.end(), [] (tuple<vector<int>, double, double> a,
		tuple<vector<int>, double, double> b) -> bool
		{
			return get<1>(b) < get<1>(a);
		});
}
