#!/bin/bash
cd ./bin
for q in 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 # prob transaction
do
  for p in 0.9 # prob write
  do
    for t in 800 # threads 
    do
      for d in 8000 # D
      do
        for l in 3 # L
        do
          for b in 6 # budget
          do
            for c in 1 # time
            do
              ./HTMSim THREADS $t BUDGET $b D $d PW $p L $l PT $q C $c SAMPLES 12000 &
            done
          done
        done
      done
    done
  done
done 

wait ;

for q in 0.9 # prob transaction
do
  for p in 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 # prob write # 0.9 done previously
  do
    for t in 800 # threads 
    do
      for d in 8000 # D
      do
        for l in 3 # L
        do
          for b in 6 # budget
          do
            for c in 1 # time
            do
              ./HTMSim THREADS $t BUDGET $b D $d PW $p L $l PT $q C $c SAMPLES 12000 &
            done
          done
        done
      done
    done
  done
done 

wait ;

for q in 0.9 # prob transaction
do
  for p in 0.9 # prob write
  do
    for t in 10 200 400 600 700 800 900 1000 # threads 
    do
      for d in 4000 # D
      do
        for l in 2 # L
        do
          for b in 6 # budget
          do
            for c in 1 # time
            do
              ./HTMSim THREADS $t BUDGET $b D $d PW $p L $l PT $q C $c SAMPLES 12000 &
            done
          done
        done
      done
    done
  done
done 

wait ;

for q in 0.9 # prob transaction
do
  for p in 0.9 # prob write
  do
    for t in 800 # threads 
    do
      for d in 2000 2500 3000 3500 4000 5000 6000 7000 8000 # D
      do
        for l in 2 # L
        do
          for b in 6 # budget
          do
            for c in 1 # time
            do
              ./HTMSim THREADS $t BUDGET $b D $d PW $p L $l PT $q C $c SAMPLES 8000 &
            done
          done
        done
      done
    done
  done
done 

wait ;

for q in 0.9 # prob transaction
do
  for p in 0.9 # prob write
  do
    for t in 800 # threads 
    do
      for d in 6000 # D
      do
        for l in 2 # L
        do
          for b in 1 2 3 4 5 7 8 9 10 # budget # 6 is done
          do
            for c in 1 # time
            do
              ./HTMSim THREADS $t BUDGET $b D $d PW $p L $l PT $q C $c SAMPLES 12000 &
            done
          done
        done
      done
    done
  done
done 

wait ;
wait ;
