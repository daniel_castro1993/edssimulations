#!/bin/bash
cd ./bin
for q in 0.9 0.5 # prob transaction
do
  for p in 1 0.5 # prob write
  do
    for d in 1024 2048 8192 # D
    do
      for l in 2 5 10 15 # L
      do
        for t in 2 4 6 8 12 16 # threads
        do
          for b in 3 5 7 9 # budget
          do
            for c in 1 4 8 # time
            do
              ./HTMSim SAMPLES 5000 THREADS $t BUDGET $b D $d PW $p L $l PT $q CT $c CN $c CS $c &
            done
          done
        done
        wait ;
      done
    done
  done
  wait ;
done
