#include "SimState.hpp"

void SimState::CleanUpAccessed(lpid_s id, map<int, ACCESS_T> &acquired)
{
	map<int, ACCESS_T>::iterator it,
			begin = acquired.begin(),
			end = acquired.end();
	
	for (it = begin; it != end; ++it) {
		int g = it->first;
		map<int, map<lpid_s, ACCESS_T> >::iterator find;
		map<lpid_s, ACCESS_T>::iterator find_me;

		find = accessed.find(g);
		find_me = find->second.find(id);
		if (find_me != find->second.end()) {
			find->second.erase(find_me);
		}

		if (find->second.empty()) {
			accessed.erase(find);
		}
	}
}
