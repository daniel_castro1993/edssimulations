#include "app.hpp"

void InitEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
	SimState *st = dynamic_cast<SimState*> (state);
	RandomLib *r = RandomLib::getInstance(0);
	double next_ts = NEXT_TIMESTAMP(now);
	AcquireEvent *acqEV;
	int g;
	ACCESS_T access;

	if (tp->IsReset() && tp->nb_sample > st->tot_nb_samples && !tp->IsEnd()) {
		printf("[LP %i] total: %i, commits: %i, aborts: %i, "
			"retries: %.3f (?%.3f)\n",
			lp->GetId(), tp->nb_sample, tp->nb_commits, tp->nb_aborts,
			tp->retries.avg, sqrt(tp->retries.sq_stddev));
		tp->End();
	}

	tp->nb_sample++;

	new_granules(tp, r, st->avg_acquisitions, st->tot_nb_granules);
	new_time(tp, r, st->avg_completion_time);

	acqEV = NEW_EVENT(AcquireEvent,
					lp->GetId(),
					next_ts,
					E_TAG_ACQUIRE);

	g = r->UniformIntDist(1, st->tot_nb_granules);
	access = r->BernoulliDist(st->prob_write) ? WRITE : READ;

	acqEV->g = g;
	acqEV->access = access;

	if (!is_abort) {
		tp->count_retries = 0;
	} else {
		tp->count_retries++;
	}

	schedule_event(lp->GetId(), acqEV);
}
