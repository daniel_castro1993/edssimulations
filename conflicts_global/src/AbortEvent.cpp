#include "app.hpp"

void AbortEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
	SimState *st = dynamic_cast<SimState*> (state);

	InitEvent *initEV;

	tp->nb_aborts++;

	st->CleanUpAccessed(lp->GetId(), tp->acquired);

	unschedule_all_events(lp->GetId());

	initEV = NEW_EVENT(InitEvent,
					lp->GetId(),
					NEXT_TIMESTAMP(now),
					0);
	initEV->is_abort = true;

	schedule_event(lp->GetId(), initEV);

	tp->acquired.clear();
}
