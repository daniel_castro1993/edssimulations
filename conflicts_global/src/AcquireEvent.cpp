#include "app.hpp"

static void check_conflicts(ThreadProcess*, SimState*,
							simtime_s, int, ACCESS_T);

void AcquireEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess* tp = dynamic_cast<ThreadProcess*> (lp);
	SimState* st = dynamic_cast<SimState*> (state);

	RandomLib *r = RandomLib::getInstance(0);
	double lat = r->ExpDist(1.0 / tp->per_granule_time);
	double next_ts = NEXT_TIMESTAMP(now + lat);
	AcquireEvent *acqEV;
	InitEvent *initEV;
	int next_g;
	ACCESS_T next_access;

	if (tp->acquired.size() < tp->nb_acqs) {

		acqEV = NEW_EVENT(AcquireEvent,
						lp->GetId(),
						next_ts,
						E_TAG_ACQUIRE);

		next_access = r->BernoulliDist(st->prob_write) ? WRITE : READ;
		do {
			next_g = r->UniformIntDist(1, st->tot_nb_granules);
		}
		while (tp->acquired.find(next_g) != tp->acquired.end());
		acqEV->g = next_g;
		acqEV->access = next_access;

		tp->acquired[g] = access; // this g, not the next one
		check_conflicts(tp, st, now, g, access);

		schedule_event(lp->GetId(), acqEV);
	}
	else {
		// COMMIT
		
		tp->nb_commits++;
		tp->retries += tp->count_retries;
		
		st->CleanUpAccessed(lp->GetId(), tp->acquired);
		tp->acquired.clear();

		initEV = NEW_EVENT(InitEvent,
						lp->GetId(),
						NEXT_TIMESTAMP(now),
						0);
		initEV->is_abort = false;

		schedule_event(lp->GetId(), initEV);
	}
}

static void check_conflicts(ThreadProcess *tp,
							SimState *st,
							simtime_s now,
							int g,
							ACCESS_T access)
{
	map<int, map<lpid_s, ACCESS_T> >::iterator find;
	AbortEvent *abortEV;

	find = st->accessed.find(g);

	if (find != st->accessed.end()) {
		map<lpid_s, ACCESS_T>::iterator it,
				begin = find->second.begin(),
				end = find->second.end();

		for (it = begin; it != end; ++it) {
			if (it->first != tp->GetId() &&
				(it->second == WRITE || access == WRITE)) {

				//ABORT 
				
				abortEV = NEW_EVENT(AbortEvent,
									tp->GetId(),
									NEXT_TIMESTAMP(now),
									0);

				schedule_event(it->first, abortEV);
			}
		}
	}
	st->accessed[g][tp->GetId()] = access;
}
