#ifndef UTILS_HPP
#define UTILS_HPP

#include "EventDrivenSimulator.hpp"

#define UPPER_BOUND_RATIO    100
#define DROP_FIRST_SAMPLES   50000

#define new_granules(tp, rand, avg_acq, max_acq) \
    tp->nb_acqs = fmax(1.0f, rand->ExpDist(1.0 / avg_acq));\
    if (tp->nb_acqs > max_acq) {\
        tp->nb_acqs = max_acq;\
    }

#define new_time(tp, rand, avg_compl_time) \
    tp->per_granule_time = rand->ExpDist(1.0 / avg_compl_time) / tp->nb_acqs;\
    if (tp->nb_acqs > UPPER_BOUND_RATIO * avg_compl_time) {\
        tp->nb_acqs = UPPER_BOUND_RATIO * avg_compl_time;\
    }

#define INJECT_CONSTRUCTORS(type)\
	type(lpid_s id, simtime_s ts) : EventHandler(id, ts) { };\
	type(lpid_s id, simtime_s ts, int tags) : EventHandler(id, ts, tags) { };

enum EVENTS_TAGS
{
	E_TAG_ACQUIRE = 0b0001
};

enum ACCESS_T
{
	WRITE, READ
};

#endif /* UTILS_HPP */

