#ifndef SIMSTATE_HPP
#define SIMSTATE_HPP

#include "utils.hpp"

#include <string>
#include <cstdlib>
#include <cstdio> 
#include <map>

using namespace sim;
using namespace std;

class SimState : public GlobalState
{
public:
	int seed, nb_lps, tot_nb_granules, tot_nb_samples;
	double avg_acquisitions, avg_completion_time, prob_write;
	string file_name;
	FILE* file_ptr;
	map<int, map<lpid_s, ACCESS_T>> accessed;
	
	void CleanUpAccessed(lpid_s, map<int, ACCESS_T>&);
};

#endif /* SIMSTATE_HPP */

