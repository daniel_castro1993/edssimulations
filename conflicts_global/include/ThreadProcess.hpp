#ifndef THREADPROCESS_HPP
#define THREADPROCESS_HPP

#include "utils.hpp"

using namespace sim;

class ThreadProcess : public LogicalProcess
{
public:
	map<int, ACCESS_T> acquired;
	int nb_acqs;
	double per_granule_time;
	int nb_sample;
	int nb_commits, nb_aborts, count_retries;
	avg_s retries;

	using LogicalProcess::LogicalProcess;
	
	virtual void CustomReset();
};

#endif /* THREADPROCESS_HPP */

