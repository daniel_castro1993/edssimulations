#ifndef CHECKCONFLICTEVENT_HPP
#define CHECKCONFLICTEVENT_HPP

#include "utils.hpp"

using namespace sim;

class CheckConflictEvent : public EventHandler
{
public:
	int g;
	ACCESS_T access;

	INJECT_CONSTRUCTORS(CheckConflictEvent)

	void Execute(LogicalProcess*, GlobalState*);
};

#endif /* CHECKCONFLICTEVENT_HPP */

