#ifndef INITEVENT_HPP
#define INITEVENT_HPP

#include "utils.hpp"

using namespace sim;

class InitEvent : public EventHandler
{
public:
	bool is_abort;

	INJECT_CONSTRUCTORS(InitEvent)

	void Execute(LogicalProcess*, GlobalState*);
};


#endif /* INITEVENT_HPP */

