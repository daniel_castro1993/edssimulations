#ifndef SIMSTATE_HPP
#define SIMSTATE_HPP

#include "utils.hpp"

#include <string>
#include <cstdlib>
#include <cstdio> 

class SimState : public sim::GlobalState
{
public:
	int seed, nb_lps, tot_nb_granules, tot_nb_samples;
	double avg_acquisitions, avg_completion_time, prob_write;
	std::string file_name;
	FILE* file_ptr;
};

#endif /* SIMSTATE_HPP */

