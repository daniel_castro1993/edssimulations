#include "app.hpp"

void AcquireEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess* tp = dynamic_cast<ThreadProcess*> (lp);
	SimState* st = dynamic_cast<SimState*> (state);
	
	RandomLib *r = RandomLib::getInstance(0);
	double lat = r->ExpDist(1.0 / tp->per_granule_time);
	double next_ts = NEXT_TIMESTAMP(now + lat);
	AcquireEvent *acqEV;
	CheckConflictEvent *chkEV;
	InitEvent *initEV;
	int next_g;
	ACCESS_T next_access;

	if (tp->acquired.size() < tp->nb_acqs) {

		acqEV = NEW_EVENT(AcquireEvent,
						lp->GetId(),
						next_ts,
						E_TAG_ACQUIRE);

		next_access = r->BernoulliDist(st->prob_write) ? WRITE : READ;
		do {
			next_g = r->UniformIntDist(1, st->tot_nb_granules);
		}
		while (tp->acquired.find(next_g) != tp->acquired.end());
		acqEV->g = next_g;
		acqEV->access = next_access;

		tp->acquired[g] = access; // this g, not the next one

		chkEV = NEW_EVENT(CheckConflictEvent,
						lp->GetId(),
						NEXT_TIMESTAMP(now),
						0);

		chkEV->g = g;
		chkEV->access = access;
		broadcast_event(chkEV);

		schedule_event(lp->GetId(), acqEV);
	}
	else {
		tp->nb_commits++;
		tp->retries += tp->count_retries;
		tp->acquired.clear();

		initEV = NEW_EVENT(InitEvent,
						lp->GetId(),
						NEXT_TIMESTAMP(now),
						0);
		initEV->is_abort = false;

		schedule_event(lp->GetId(), initEV);
	}
}
