#include "app.hpp"

void CheckConflictEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
	ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
	SimState *st = dynamic_cast<SimState*> (state);
	InitEvent *initEV;
	map<int, ACCESS_T>::iterator it = tp->acquired.find(g),
			end = tp->acquired.end();

	if (lp->GetId() == scheduler_process) {
		return;
	}

	if (it != end && (it->second == WRITE || access == WRITE)) {
		tp->nb_aborts++;
		tp->count_retries++;
		tp->acquired.clear();
		unschedule_all_events(lp->GetId());

		initEV = NEW_EVENT(InitEvent,
						lp->GetId(),
						NEXT_TIMESTAMP(now),
						0);
		initEV->is_abort = true;

		schedule_event(lp->GetId(), initEV);
	}
}
