#include "app.hpp"

using namespace sim;

void ThreadProcess::CustomReset()
{
	retries.count = 0;
	nb_commits = 0;
	nb_aborts = 0;
	nb_sample = 0;
}
