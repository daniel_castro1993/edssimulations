#include "app.hpp"

#include <cstdlib>
#include <cstdio> 
#include <ctime>

using namespace sim;
using namespace std;

extern int reset_threshold;

// entry point for the simulation

void init(map<string, string>& args)
{
	ThreadProcess *tLP;
	InitEvent *initEV;
	SimState *state = new SimState();
	int i;
	lpid_s lpid;

	SET_STATE(SimState, state);
	reset_threshold = DROP_FIRST_SAMPLES;

	srand(time(NULL));

	state->seed = rand();
	state->nb_lps = 10;
	state->tot_nb_granules = 1000;
	state->tot_nb_samples = 10;
	state->avg_acquisitions = 10;
	state->avg_completion_time = 1;
	state->prob_write = 1.0;
	state->file_name = "conflits.txt";

	if (!args["DEFAULT_SEED"].empty()) {
		state->seed = stoi(args["DEFAULT_SEED"]);
	}

	if (!args["P_W"].empty()) {
		state->prob_write = stod(args["P_W"]);
	}

	if (!args["LPS"].empty()) {
		state->nb_lps = stoi(args["LPS"]);
	}

	if (!args["POOL_SIZE"].empty()) {
		state->tot_nb_granules = stoi(args["POOL_SIZE"]);
	}

	if (!args["AVG_TIME"].empty()) {
		state->avg_completion_time = stod(args["AVG_TIME"]);
	}

	if (!args["AVG_ACQS"].empty()) {
		state->avg_acquisitions = stod(args["AVG_ACQS"]);
	}

	if (!args["SAMPLES"].empty()) {
		state->tot_nb_samples = stoi(args["SAMPLES"]);
	}

	if (!args["FILE"].empty()) {
		state->file_name = args["FILE"];
	}

	if (state->tot_nb_granules < state->avg_acquisitions) {
		printf("POOL_SIZE must be greater than AVG_ACQS\n");
		exit(EXIT_FAILURE);
	}

	printf("Started simulation with parameters: \nLPS = %i\nP_W = %.2f\n"
		"DEFAULT_SEED = %i\nPOOL_SIZE = %i\nAVG_TIME = %f\nAVG_ACQS = %f\n"
		"SAMPLES = %i\n", state->nb_lps, state->prob_write, state->seed,
		state->tot_nb_granules, state->avg_completion_time,
		state->avg_acquisitions, state->tot_nb_samples);

	RandomLib::create(state->seed);
	for (i = 0; i < state->nb_lps; ++i) {
		tLP = new ThreadProcess();
		lpid = add_logical_process(tLP);
		initEV = new InitEvent(lpid, NEXT_TIMESTAMP(0));
		initEV->is_abort = false;
		schedule_event(lpid, initEV);
	}
}

void end(vector<LogicalProcess*> lps, GlobalState *state)
{
	vector<LogicalProcess*>::iterator it,
			begin = lps.begin(),
			end = lps.end();
	int tot_lps = lps.size(), tot_commits = 0, tot_aborts = 0;
	double tot_avg_retries = 0, prob_abort = 0;

	SimState *st = dynamic_cast<SimState*> (state);

	st->file_ptr = fopen(st->file_name.c_str(), "w");
	for (it = begin; it != end; ++it) {
		ThreadProcess* tp = dynamic_cast<ThreadProcess*> (*it);
		tot_avg_retries += tp->retries.avg;
		tot_commits += tp->nb_commits;
		tot_aborts += tp->nb_aborts;
	}
	prob_abort = (double) tot_aborts / (double) (tot_aborts + tot_commits);
	tot_avg_retries /= tot_lps;
	fprintf(st->file_ptr, "%12s,%12s,%12s,%12s,%12s,%12s,%12s,%12s,%12s\n",
			"SEED", "SAMPLES", "P_W", "LPS", "POOL_SIZE", "AVG_TIME",
			"AVG_ACQ", "P_A (%)", "AVG_RETRIES");
	fprintf(st->file_ptr, "%12i,%12i,%12.3f,%12i,%12i,%12f,%12f.2,%12f,%12f\n",
			st->seed, st->tot_nb_samples, st->prob_write, st->nb_lps,
			st->tot_nb_granules, st->avg_completion_time, st->avg_acquisitions,
			prob_abort * 100.0, tot_avg_retries);
	fclose(st->file_ptr);
}
